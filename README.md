# CodeDesigner

#### 项目介绍
IDEA intellij插件，通过数据库连接，获取表结构，自动生成代码文件，使用freemarker自定义模板。

1. 支持mysql数据库；
2. 支持自定义模板，使用freemarker解析；
3. 支持模板生成过程中使用自定义参数。


#### 安装教程

1. 在setting-plugins搜索CodeDesigner安装；
2. 下载附件中打包好的压缩包，Intellij中setting-plugins选择从本地安装，选择刚才下载的压缩包，安装后重启即可；

#### 使用说明

1. 添加数据库连接：
![添加数据源](https://gitee.com/uploads/images/2018/0701/094429_bd40276e_1147334.png "添加数据源.png")

2. 可以查看模板生成参数和示例，来了解模板编写过程中可以使用的参数：
![代码生成参数](https://gitee.com/uploads/images/2018/0701/094821_87ce5131_1147334.png "参数查看.png")

3. 设置自定义参数，代码模板中可以使用：
![设置自定义参数](https://gitee.com/uploads/images/2018/0701/094616_be42e956_1147334.png "设置通用参数.png")

4. 设置代码生成模板：
![设置模板](https://gitee.com/uploads/images/2018/0701/094511_35ec3590_1147334.png "添加模板.png")

5. 打开CodeDesigner:
1) 在项目中右键后选择：

2) Tools菜单中选择：

![打开方式1](https://gitee.com/uploads/images/2018/0701/095339_c1b4f9b5_1147334.png "打开方式1.png")

3) 编辑器中Alt+Insert打开：

![打开方式2](https://gitee.com/uploads/images/2018/0701/095446_5bfda0e6_1147334.png "打开方式2.png")

6. CodeDesigner:
![代码生成main](https://gitee.com/uploads/images/2018/0701/095528_b26a36ec_1147334.png "代码生成main.png")

7. 默认设置界面：
1) 可以设置代码编写者；
2）容许去除数据库的前缀符；
3) 代码输出路径，默认为项目根目录，容许选择其他路径；

8. 表和字段设置：
![表和字段设置](https://gitee.com/uploads/images/2018/0701/095923_cb9ab9c8_1147334.png "表和字段设置.png")

1) 下拉框选择配置的数据源，点击连接按钮，获取数据库所有表元数据；

2) 表的部分元数据容许修改：
![表可以修改](https://gitee.com/uploads/images/2018/0701/100219_b475acde_1147334.png "表可以修改.png")

 **注意** ：修改后一定要勾选对应行的单元框，点击保存表设置按钮，此时才会真正保存修改后的数据；

3) 点击查看表字段按钮，可以查看修改对应表的所有字段元数据：
![字段修改](https://gitee.com/uploads/images/2018/0701/100531_b38ddb32_1147334.png "字段修改.png")

**注意** ：修改后，点击设置按钮，此时才会真正保存修改后的数据；

4) 点击下一步按钮；

9. 选择代码生成模板,点击生成按钮，此时将会对之前选择的表按照模板生成代码：

![生成代码](https://gitee.com/uploads/images/2018/0701/100810_96ea12ee_1147334.png "生成代码.png")

1) 生成模板支持多选；

2) 代码生成后会打开所有生成文件。


#### 功能缺失

1. 刚开始想通过intellij DatabaseTools插件的扩展点，来获取数据源的配置，后来没找到这个扩展点，只好选择经常使用的mysql数据库，如果有哪位了解的，有缘看到这里的，请一定要告诉我，让我学习下；

2. 模板配置使用freemarker，在编写模板后，无法验证模板的正确性；

3. 生成模板选择界面，原本是想，在选择模板后，可以展示生成后文件内容，但是因为要支持数据库表的多选，没想到好的展示方式，放弃了；


#### 感谢

在插件编写过程中参考了2位哥们的代码，这里感谢下：

1. intellij插件AutoGenerator，他是使用sql来生成阿里的tddl的代码，跟我的生成源选择不一样，我是根据数据库表生成：
![autoGeerator](https://gitee.com/uploads/images/2018/0701/102030_1f442326_1147334.png "autoGeerator.png")

2. rapid-generator框架，之前写过一篇blog解析代码生成部分的源码，这个框架支持的功能相当丰富，还未能看到超越这个框架的，可以搜索学习。

