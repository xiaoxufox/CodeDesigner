package com.fox.plugin.codedesigner.util;

import java.nio.charset.Charset;

import org.jetbrains.annotations.NotNull;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.CharsetToolkit;
import com.intellij.openapi.vfs.encoding.EncodingManager;
import com.intellij.openapi.vfs.encoding.EncodingProjectManager;

/**
 * 用于获取一些环境配置
 *
 * @author copy from others
 */
public class EnvUtil {
    public static Project project;

    public static Charset encodeTo = Charset.forName("UTF-8");
    public static Charset encodeFrom = Charset.forName("UTF-8");

    @NotNull
    public static Charset getProjectCharset() {
        try {
            return EncodingProjectManager.getInstance(project).getDefaultCharset();
        }catch (Exception e){
            try{
                return EnvUtil.project.getBaseDir().getCharset();
            }catch (Exception e1){
                return Charset.forName("UTF-8");
            }
        }
    }

    @NotNull
    public static Charset getIDECharset() {
        Charset ideCharset;
        try {
            ideCharset = EncodingManager.getInstance().getDefaultCharset();
        }catch (Exception e){
            try {
                ideCharset = CharsetToolkit.getDefaultSystemCharset();
            }catch (Exception e1){
                ideCharset = Charset.forName("UTF-8");
            }
        }
        return ideCharset;
    }

    @NotNull
    public static Charset getCharsetFromEncoding(String encoding) {
        try {
            return Charset.forName(encoding);
        }catch (Exception e){
            return Charset.defaultCharset();
        }
    }
}
