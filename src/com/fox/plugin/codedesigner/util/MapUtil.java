package com.fox.plugin.codedesigner.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

/**
 * MapUtil<br>
 *
 * @author fox
 */
public class MapUtil {

    public static Map resolveKeyPlaceholder(Properties properties) {
        Map systemParma = new HashMap();
        for(Iterator it = properties.entrySet().iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry)it.next();
            systemParma.put(((String)entry.getKey()).replaceAll(".", "-"), entry.getValue());
        }
        return systemParma;
    }

    public static Map resolveKeyPlaceholder(Map properties) {
        Map systemParma = new HashMap();
        for(Iterator it = properties.entrySet().iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry)it.next();
            systemParma.put(((String)entry.getKey()).replaceAll(".", "-"), entry.getValue());
        }
        return systemParma;
    }
}
