package com.fox.plugin.codedesigner.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fox.plugin.codedesigner.setting.SettingFacade;

/**
 * StringUtil<br>
 *
 * @author fox
 */
public class StringUtil {

    public StringUtil() {
    }

    public static boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * 将表名转为类名
     */
    public static String toJavaClassName(String sqlName) {
        String processedSqlName = removeTableSqlNamePrefix(sqlName);
        // 复数形式转为单数
        //processedSqlName = singularize(processedSqlName);
        return makeAllWordFirstLetterUpperCase(toUnderscoreName(processedSqlName));
    }

    public static void main(String[] args){
        System.out.println(makeAllWordFirstLetterUpperCase(toUnderscoreName("status")));
    }

    /**
     * 词组首字母大写
     * exp: User_Info->UserInfo
     */
    public static String makeAllWordFirstLetterUpperCase(String sqlName) {
        String[] strs = sqlName.toLowerCase().split("_");
        String result = "";
        String preStr = "";
        for(int i = 0; i < strs.length; i++) {
            if(preStr.length() == 1) {
                result += strs[i];
            }else {
                result += capitalize(strs[i]);
            }
            preStr = strs[i];
        }
        return result;
    }

    public static String capitalize(String str) {
        return changeFirstCharacterCase(str, true);
    }

    public static String uncapitalize(String str) {
        return changeFirstCharacterCase(str, false);
    }

    private static String changeFirstCharacterCase(String str, boolean capitalize) {
        if (str == null || str.length() == 0) {
            return str;
        }
        StringBuffer buf = new StringBuffer(str.length());
        if (capitalize) {
            buf.append(Character.toUpperCase(str.charAt(0)));
        }
        else {
            buf.append(Character.toLowerCase(str.charAt(0)));
        }
        buf.append(str.substring(1));
        return buf.toString();
    }

    /**
     * 将一个单词从复数转变为单数, 如 customers => customer
     */
    public static String singularize(String word) {
        return Inflector.getInstance().singularize(word);
    }

    /**
     * Convert a name in camelCase to an underscored name in lower case.
     * Any upper case letters are converted to lower case with a preceding underscore.
     * @param name the string containing original name
     * @return the converted name
     */
    public static String toUnderscoreName(String name) {
        if(name == null) {
            return null;
        }

        String filteredName = name;
        if(filteredName.indexOf("_") >= 0 && filteredName.equals(filteredName.toUpperCase())) {
            filteredName = filteredName.toLowerCase();
        }
        if(filteredName.indexOf("_") == -1 && filteredName.equals(filteredName.toUpperCase())) {
            filteredName = filteredName.toLowerCase();
        }

        StringBuffer result = new StringBuffer();
        if (filteredName != null && filteredName.length() > 0) {
            result.append(filteredName.substring(0, 1).toLowerCase());
            for (int i = 1; i < filteredName.length(); i++) {
                String preChart = filteredName.substring(i - 1, i);
                String c = filteredName.substring(i, i + 1);
                if(c.equals("_")) {
                    result.append("_");
                    continue;
                }
                if(preChart.equals("_")){
                    result.append(c.toLowerCase());
                    continue;
                }
                if(c.matches("\\d")) {
                    result.append(c);
                }else if (c.equals(c.toUpperCase())) {
                    result.append("_");
                    result.append(c.toLowerCase());
                }
                else {
                    result.append(c);
                }
            }
        }
        return result.toString();
    }

    /**
     * 移除表名前缀
     */
    public static String removeTableSqlNamePrefix(String sqlName) {
        String[] prefixs = getTableRemovePrefix();
        for(String prefix : prefixs) {
            String removedPrefixSqlName = removePrefix(sqlName, prefix,true);
            if(!removedPrefixSqlName.equals(sqlName)) {
                return removedPrefixSqlName;
            }
        }
        return sqlName;
    }

    /**
     * 移除前缀string
     */
    public static String removePrefix(String str,String prefix,boolean ignoreCase) {
        if(str == null){
            return null;
        }

        if(prefix == null){
            return str;
        }

        if(ignoreCase) {
            if(str.toLowerCase().startsWith(prefix.toLowerCase())) {
                return str.substring(prefix.length());
            }
        }else {
            if(str.startsWith(prefix)) {
                return str.substring(prefix.length());
            }
        }
        return str;
    }

    /**
     * 获取配置的去除前缀string
     */
    public static String[] getTableRemovePrefix(){
        String tableRemovePrefix = SettingFacade.getGeneratorDefault().getTableRemovePrefix();
        if (isEmpty(tableRemovePrefix)){
            return new String[0];
        }

        return tableRemovePrefix.split("\\|");
    }

    /**
     * 生成 路径值,如 pkg=com.company.project 将生成 pkg_dir=com/company/project的值
     */
    public static Map getDirValuesMap(Map<String, Object> map) {
        Map<String, Object> dirValues = new HashMap<String, Object>();

        for (Map.Entry<String, Object> entry : map.entrySet()){
            String key = entry.getKey();
            Object value = entry.getValue();
            if(value instanceof String) {
                String dirKey = key+"_dir";
                String dirValue = value.toString().replace('.', '/');
                dirValues.put(dirKey, dirValue);
            }
        }

        return dirValues;
    }

    /**
     * 获取文件扩展名
     */
    public static String getExtension(String filename) {
        if (filename == null) {
            return null;
        }
        int index = filename.indexOf(".");
        if (index == -1) {
            return "";
        } else {
            return filename.substring(index + 1);
        }
    }

    /**
     * 获取文件名
     */
    public static String getFileNameNoExtension(String filename, List<String> extensionList) {
        if (filename == null) {
            return null;
        }

        String fileNameDealed = filename;

        for (String extension : extensionList){
            if (filename.endsWith("."+extension)){
                fileNameDealed = filename.substring(0, filename.indexOf("."+extension));
            }
        }

        return fileNameDealed;
    }

    /**
     * 获取文件名
     */
    public static String getFileNameRemoveSplit(String fileName, List<String> splitList) {
        if (fileName == null) {
            return null;
        }

        String fileNameDealed = fileName;

        for (String split : splitList){
            int index = fileName.lastIndexOf(split);
            if (index != -1) {
                fileNameDealed = fileName.substring(index+1);
                break;
            }
        }

        return fileNameDealed;
    }

}
