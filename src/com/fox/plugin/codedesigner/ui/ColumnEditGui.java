package com.fox.plugin.codedesigner.ui;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import com.fox.plugin.codedesigner.db.setting.constant.ColumnEnum;
import com.fox.plugin.codedesigner.db.setting.constant.TableEnum;
import com.fox.plugin.codedesigner.db.table.model.Column;
import com.fox.plugin.codedesigner.db.table.model.Table;
import com.intellij.openapi.editor.colors.EditorColorsManager;
import com.intellij.openapi.editor.colors.EditorColorsScheme;
import com.intellij.ui.table.JBTable;

/**
 * ColumnEditGui<br>
 * 用于查看编辑表字段
 *
 * @author fox
 */
public class ColumnEditGui  extends JDialog {

    private static final long serialVersionUID = -3631516638053056838L;


    private JPanel mainPanel;
    private JScrollPane columnScrollPanel;
    private JButton columnApplyBtn;

    /**
     * 样式
     */
    EditorColorsScheme colorScheme;
    Color background;

    private CodeDesignerDialog codeDesignerDialog;
    private Table currentTable;

    /**
     * 数据库表字段-表头信息
     */
    private static String[] dbColums = ColumnEnum.getColumnEnums();

    /**
     * 数据库展示字段table
     */
    JBTable columnTable;

    public ColumnEditGui(CodeDesignerDialog codeDesignerDialog, Table currentTable) {
        this.codeDesignerDialog = codeDesignerDialog;
        this.currentTable = currentTable;

        /*  dialog样式等设置 */
        colorScheme = EditorColorsManager.getInstance().getGlobalScheme();
        background = colorScheme.getDefaultBackground();
        this.setContentPane(mainPanel);
        this.setTitle("编辑表"+currentTable.getSqlName().trim());
        this.setModal(true);
        this.setBackground(background);
        this.setSize(800, 600);
        this.setLocationRelativeTo(null);

        // 初始化字段
        initColumnTable(currentTable);

        columnApplyBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modifyTableColumn(e);
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        mainPanel.registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * 初始化字段
     */
    private void initColumnTable(Table currentTable) {
        java.util.List<Column> columnList = currentTable.getColumns();

        int num = 0;
        java.util.List<Object[]> list2 = null;

        if (columnList != null && !columnList.isEmpty()) {
            list2 = new ArrayList<Object[]>();
            for (Column column : columnList) {
                java.util.List<Object> list1 = new ArrayList<Object>();
                num++;
                list1.add(num);
                list1.add(column.getSqlName());
                list1.add(column.getSqlTypeName());
                list1.add(column.isPk() ? "Y" : "");
                list1.add(column.getRemarks());
                list1.add(column.getColumnName());
                list1.add(column.getJavaType());
                list1.add(column.getColumnNameLower());

                list2.add(list1.toArray(new Object[]{list1.size()}));
            }
        }
        Object[][] data = null;
        int height = 5;
        if (list2 != null && !list2.isEmpty()){
            height = list2.size();
            data = list2.toArray(new Object[][]{new Object[]{list2.size()}});
        }
        columnScrollPanel.setSize((int) columnScrollPanel.getSize().getWidth(), (int) columnScrollPanel.getSize().getHeight() * height / 4);

        TableModel model = new DefaultTableModel(data, dbColums){
            private static final long serialVersionUID = -8310812824505049103L;

            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) {
                //相应的列不可编辑
                if (colIndex == 0 || colIndex == 1 || colIndex == 2 || colIndex == 3) {
                    return false;
                }
                return true;
            }
        };
        columnTable = new JBTable(model);
        columnTable.setBackground(background);
        columnTable.getTableHeader().setReorderingAllowed(false);

        //No.列尝试不同的前景色
        TableColumn tableColumn = columnTable.getColumn(TableEnum.No.getName());
        DefaultTableCellRenderer cellRender = new DefaultTableCellRenderer();
        cellRender.setHorizontalAlignment(SwingConstants.CENTER);
        tableColumn.setCellRenderer(cellRender);
        tableColumn.setPreferredWidth(15);

        columnScrollPanel.getViewport().add(columnTable);
    }

    /**
     * 保存所有对字段的修改
     */
    private void modifyTableColumn(ActionEvent e) {
        // 获取所有字段
        DefaultTableModel tm = (DefaultTableModel)columnTable.getModel();
        int count = tm.getRowCount();

        // 没有数据就return
        if (count < 0){
            onCancel();
        }

        // 这里处理成所有都容许修改
        for(int i=0;i<count;i++){
            String sqlName = String.valueOf(tm.getValueAt(i, ColumnEnum.SqlName.getOrder())).trim();

            Column column = this.currentTable.getColumnMap().get(sqlName.trim());

            column.setRemarks(String.valueOf(tm.getValueAt(i, ColumnEnum.Remarks.getOrder())).trim());
            column.setColumnName(String.valueOf(tm.getValueAt(i, ColumnEnum.ColumnName.getOrder())).trim());
            column.setJavaType(String.valueOf(tm.getValueAt(i, ColumnEnum.JavaType.getOrder())).trim());
            column.setColumnNameLower(String.valueOf(tm.getValueAt(i, ColumnEnum.ColumnNameLower.getOrder())).trim());
        }

        onCancel();
    }

    private void onCancel() {
        dispose();
    }
}
