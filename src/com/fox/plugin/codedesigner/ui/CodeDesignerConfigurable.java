package com.fox.plugin.codedesigner.ui;

import javax.swing.*;
import java.util.Map;

import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.fox.plugin.codedesigner.setting.CodeTemplateSettings;
import com.fox.plugin.codedesigner.setting.DataSourceSettings;
import com.fox.plugin.codedesigner.setting.GeneratorDefaultSettings;
import com.fox.plugin.codedesigner.setting.model.CodeTemplateModel;
import com.fox.plugin.codedesigner.setting.model.DataSourceModel;
import com.fox.plugin.codedesigner.setting.model.GeneratorDefaultModel;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SearchableConfigurable;

/**
 * CodeDesignerConfigurable<br>
 * Setting配置
 *
 * @author fox
 */
public class CodeDesignerConfigurable implements SearchableConfigurable {

    private DataSourceSettings dataSourceSettings;
    private CodeTemplateSettings codeTemplateSettings;
    private GeneratorDefaultSettings generatorDefaultSettings;

    private CodeDesignerConfigurationGui configuration;

    public CodeDesignerConfigurable() {
        dataSourceSettings = ServiceManager.getService(DataSourceSettings.class);
        codeTemplateSettings = ServiceManager.getService(CodeTemplateSettings.class);
        generatorDefaultSettings = ServiceManager.getService(GeneratorDefaultSettings.class);
    }

    @NotNull
    @Override
    public String getId() {
        return "plugin.codedesigner";
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "CodeDesigner";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return "help.codedesigner.configuration";
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        if (this.configuration == null) {
            this.configuration = new CodeDesignerConfigurationGui(dataSourceSettings, codeTemplateSettings);
        }
        return this.configuration.getMainPanel();
    }

    @Override
    public boolean isModified() {
        // 校验数据源是否有变化
        if (dataSourceSettings.getDataSourceMap().size() != configuration.getDataSourceTable().getRowCount()) {
            return true;
        }

        for (Map.Entry<String, DataSourceModel> entry : configuration.getDataSourceMap().entrySet()) {
            DataSourceModel dataSourceModel = dataSourceSettings.getDataSource(entry.getKey());
            if (dataSourceModel == null || !dataSourceModel.equals(entry.getValue())) {
                return true;
            }
        }

        // 校验模板是否有变化
        if (codeTemplateSettings.getTemplateMap().size() != configuration.getTemplateListItemCount()) {
            return true;
        }

        for (Map.Entry<String, CodeTemplateModel> entry : configuration.getTemplateMap().entrySet()) {
            CodeTemplateModel codeTemplateModel = codeTemplateSettings.getTemplate(entry.getKey());
            if (codeTemplateModel == null || !codeTemplateModel.equals(entry.getValue())) {
                return true;
            }
        }

        // 校验通用配置是否有变化
        if (generatorDefaultSettings.getGeneratorDefaultModel().getCustomerConfigMap().size() != configuration.getCustomerConfigMap().size()) {
            return true;
        }

        for (Map.Entry<String, String> entry : configuration.getCustomerConfigMap().entrySet()) {
            String value = generatorDefaultSettings.getGeneratorDefaultModel().getCustomerConfigMap().get(entry.getKey());
            if (value == null || !value.equals(entry.getValue())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void apply() throws ConfigurationException {
        dataSourceSettings.setDataSourceMap(configuration.getDataSourceMap());
        codeTemplateSettings.setTemplateMap(configuration.getTemplateMap());

        Map<String, String> customerConfig = configuration.getCustomerConfigMap();
        GeneratorDefaultModel generatorDefaultModel = generatorDefaultSettings.getGeneratorDefaultModel();
        generatorDefaultModel.setCustomerConfigMap(customerConfig);
        generatorDefaultSettings.setGeneratorDefaultModel(generatorDefaultModel);

        configuration.refresh(dataSourceSettings, codeTemplateSettings, generatorDefaultSettings);
    }

    @Override
    public void reset() {
        if (this.configuration != null) {
            this.configuration.refresh(dataSourceSettings, codeTemplateSettings, generatorDefaultSettings);
        }
    }

    @Override
    public void disposeUIResources() {
        this.configuration = null;
    }
}
