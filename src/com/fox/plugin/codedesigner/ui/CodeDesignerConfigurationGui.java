package com.fox.plugin.codedesigner.ui;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.jdesktop.swingx.JXTreeTable;

import com.fox.plugin.codedesigner.db.table.TableFactory;
import com.fox.plugin.codedesigner.setting.CodeTemplateSettings;
import com.fox.plugin.codedesigner.setting.DataSourceSettings;
import com.fox.plugin.codedesigner.setting.GeneratorDefaultSettings;
import com.fox.plugin.codedesigner.setting.SettingFacade;
import com.fox.plugin.codedesigner.setting.constant.CustomerConfigColumnEnum;
import com.fox.plugin.codedesigner.setting.constant.DataSourceColumnEnum;
import com.fox.plugin.codedesigner.setting.constant.SampleEnum;
import com.fox.plugin.codedesigner.setting.model.CodeTemplateModel;
import com.fox.plugin.codedesigner.setting.model.DataSourceModel;
import com.fox.plugin.codedesigner.setting.model.GeneratorDefaultModel;
import com.fox.plugin.codedesigner.ui.util.SampleDemoGenerator;
import com.fox.plugin.codedesigner.ui.util.SampleTreeTableModel;
import com.fox.plugin.codedesigner.util.StringUtil;
import com.intellij.openapi.editor.colors.EditorColorsManager;
import com.intellij.openapi.editor.colors.EditorColorsScheme;
import com.intellij.openapi.ui.Messages;
import com.intellij.ui.components.JBList;
import com.intellij.ui.table.JBTable;

/**
 * CodeDesignerConfigurationGui<br>
 *
 * @author fox
 */
public class CodeDesignerConfigurationGui {

    private JPanel mainPanel;
    private JTabbedPane configTablePanel;
    private JLabel configLabel;
    private JPanel dataSourceMainPanel;
    private JPanel templateMainPanel;
    private JTextField alisNameTxt;
    private JTextField urlTxt;
    private JTextField passwordTxt;
    private JTextField userTxt;
    private JButton addBtn;
    private JButton removeBtn;
    private JLabel connectionAlisNameLabel;
    private JLabel userNameLabel;
    private JLabel passwordLabel;
    private JTextField driverTxt;
    private JLabel driveClassNameLabel;
    private JPanel operationPanel;
    private JScrollPane dataSourceScrollPane;
    private JButton addTemplateBtn;
    private JButton deleteTemplateBtn;

    private JScrollPane templateScrollPanel;
    private JTextField templateTypeTxt;
    private JScrollPane templateContentScrollPanel;
    private JLabel templateTypeLbl;
    private JSplitPane templateSplitPanel;
    private JPanel templateParamPanel;
    private JScrollPane templateParamScrollPanel;
    private JPanel templateDemoPanel;
    private JSplitPane templateDemoSplitPanel;
    private JScrollPane templateDemoLeftScrollPanel;
    private JPanel templateDemoRightPanel;
    private JLabel templateTypeRightLbl;
    private JTextField templateTypeRightTxt;
    private JScrollPane templateContentRightScrollPanel;
    private JTextArea templateContentRightTxtArea;

    EditorColorsScheme colorScheme;
    Color background;

    /**
     * 数据源配置-table
     */
    JBTable dataSourceTable;

    /**
     * 数据源配置列表-表头信息
     */
    private static String[] dataSourceColNames = DataSourceColumnEnum.getColumnNames();

    JBList templateList;
    JTextArea templateContentTxtArea;
    private JPanel defaultConfigPanel;
    private JPanel customerConfigPanel;
    private JScrollPane customerConfigScrollPanel;
    private JButton configAddBtn;
    private JButton configDeleteBtn;
    private JTextField configKeyTxt;
    private JLabel configKeyLbl;
    private JLabel configValueLbl;
    private JTextField configValueTxt;
    private JButton testConnectBtn;
    private JTextField hostTxt;
    private JTextField dataBaseNameTxt;
    private JLabel hostLbl;
    private JLabel dataBaseNameLbl;
    private JLabel urlLbl;
    private JButton editTemplateBtn;

    /**
     * 数据源配置
     */
    private DataSourceSettings dataSourceSettings;

    /**
     * 模板配置
     */
    private CodeTemplateSettings codeTemplateSettings;

    /**
     * 模板配置参数展示
     */
    JXTreeTable sampleTreeTable;

    /**
     * 模板示例list
     */
    JBList templateDemoList;

    /**
     * 自定义配置-table
     */
    JBTable customerConfigTable;

    /**
     * 自定义配置-表头信息
     */
    private static String[] customerConfigColNames = CustomerConfigColumnEnum.getCustomerConfigColumns();

    public CodeDesignerConfigurationGui(DataSourceSettings dataSourceSettings, CodeTemplateSettings codeTemplateSettings) {
        this.dataSourceSettings = dataSourceSettings;
        this.codeTemplateSettings = codeTemplateSettings;

        colorScheme = EditorColorsManager.getInstance().getGlobalScheme();
        background = colorScheme.getDefaultBackground();

        // 初始化数据源配置
        Map<String, DataSourceModel> dataSourceSettingsMap = dataSourceSettings.getDataSourceMap();
        initDataSourceTable(dataSourceSettingsMap);

        hostTxt.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                String url = "jdbc:mysql://"+hostTxt.getText().trim()+"/"+dataBaseNameTxt.getText().trim();
                urlTxt.setText(url);
            }
        });

        dataBaseNameTxt.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                String url = "jdbc:mysql://"+hostTxt.getText().trim()+"/"+dataBaseNameTxt.getText().trim();
                urlTxt.setText(url);
            }
        });

        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addDataSource(e);
            }
        });

        removeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeDataSource(e);
            }
        });

        testConnectBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                testConnect(e);
            }
        });

        // 初始化模板配置
        Map<String, CodeTemplateModel> templateMap = codeTemplateSettings.getTemplateMap();
        initTemplateList(templateMap);

        addTemplateBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addTemplateGui(e);
            }
        });

        editTemplateBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editTemplateGui(e);
            }
        });

        deleteTemplateBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeTemplate(e);
            }
        });

        // 模板配置可选参数panel
        initSampleTreeTable();

        // 模板代码示例panel
        initTemplateDemo();

        // 初始化用户自定义配置
        initCustomerConfig();

        // 添加自定义配置监听
        configAddBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addCustomerConfig(e);
            }
        });

        // 删除自定义配置监听
        configDeleteBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeCustomerConfig(e);
            }
        });
    }

    /**
     * 模板代码示例panel
     */
    private void initTemplateDemo() {
        java.util.List<CodeTemplateModel> list2 = new ArrayList<CodeTemplateModel>();

        java.util.List<CodeTemplateModel> templateModelList = SampleDemoGenerator.createTemplateDemo();

        for (CodeTemplateModel templateModel : templateModelList){
            list2.add(templateModel);
        }

        templateDemoList = new JBList<CodeTemplateModel>(list2);
        // 单选
        templateDemoList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // 添加选项选中状态被改变的监听器
        templateDemoList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                CodeTemplateModel model = (CodeTemplateModel)templateDemoList.getSelectedValue();
                templateTypeRightTxt.setText(model == null ? "" : model.getType());
                templateContentRightTxtArea.setText(model == null ? "" : model.getContent().trim());
            }
        });

        // 设置默认选中项
        templateDemoList.setSelectedIndex(0);

        // 添加到内容面板容器
        templateDemoLeftScrollPanel.getViewport().add(templateDemoList);

        // 设置关联显示模板内容的textArea不容许编辑
        templateContentRightTxtArea.setEditable(false);
    }

    /**
     * 初始化dataSourceTable
     */
    private void initDataSourceTable(Map<String, DataSourceModel> dataSourceSettingsMap) {
        int num = 0;
        java.util.List<Object[]> list2 = null;

        if (dataSourceSettingsMap != null && !dataSourceSettingsMap.isEmpty()) {
            list2 = new ArrayList<Object[]>();
            for (Map.Entry<String, DataSourceModel> entry : dataSourceSettingsMap.entrySet()) {
                String aliasName = entry.getKey();
                DataSourceModel model = entry.getValue();

                java.util.List<Object> list1 = new ArrayList<Object>();
                num++;
                list1.add(num);
                list1.add(aliasName);
                list1.add(model.getHostIp());
                list1.add(model.getDbName());
                list1.add(model.getUserName());
                list1.add(model.getPassword());
                list1.add(model.getUrl());
                list1.add(model.getDriverClassName());
                list2.add(list1.toArray(new Object[]{list1.size()}));
            }
        }
        Object[][] data = null;
        int height = 5;
        if (list2 != null && !list2.isEmpty()){
            height = list2.size();
            data = list2.toArray(new Object[][]{new Object[]{list2.size()}});
        }
        dataSourceScrollPane.setSize((int) dataSourceScrollPane.getSize().getWidth(), (int) dataSourceScrollPane.getSize().getHeight() * height / 4);

        TableModel model = new DefaultTableModel(data, dataSourceColNames){
            private static final long serialVersionUID = -8310812824505049103L;

            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false;
            }
        };
        dataSourceTable = new JBTable(model);
        dataSourceTable.setBackground(background);
        dataSourceTable.getTableHeader().setReorderingAllowed(false);

        //No.列尝试不同的前景色
        TableColumn tableColumn = dataSourceTable.getColumn(DataSourceColumnEnum.No.getName());
        DefaultTableCellRenderer cellRender = new DefaultTableCellRenderer();
        cellRender.setHorizontalAlignment(SwingConstants.CENTER);
        tableColumn.setCellRenderer(cellRender);
        tableColumn.setPreferredWidth(30);

        dataSourceScrollPane.getViewport().add(dataSourceTable);
    }

    /**
     * 增加一条数据源信息
     */
    private void addDataSource(ActionEvent e) {
        if (StringUtil.isEmpty(alisNameTxt.getText()) || StringUtil.isEmpty(hostTxt.getText()) || StringUtil.isEmpty(dataBaseNameTxt.getText())
                || StringUtil.isEmpty(urlTxt.getText()) || StringUtil.isEmpty(userTxt.getText()) || StringUtil.isEmpty(passwordTxt.getText())){
            Messages.showMessageDialog("添加的数据源信息部分为空","Error", Messages.getErrorIcon());
            return;
        }

        String aliasName = alisNameTxt.getText().trim();
        Map<String, DataSourceModel> dataSourceMap = getDataSourceMap();
        if (dataSourceMap != null && dataSourceMap.get(aliasName) != null){
            Messages.showMessageDialog("新添加的数据源别名已经存在","Error", Messages.getErrorIcon());
            return;
        }

        int rowCount = dataSourceTable.getRowCount();
        DefaultTableModel dataSourceTableModel = (DefaultTableModel)dataSourceTable.getModel();

        String driverClassName = "com.mysql.jdbc.Driver";

        java.util.List<String> columList = new ArrayList<String>();
        columList.add(String.valueOf(rowCount+1));
        columList.add(aliasName);
        columList.add(hostTxt.getText().trim());
        columList.add(dataBaseNameTxt.getText().trim());
        columList.add(userTxt.getText().trim());
        columList.add(passwordTxt.getText().trim());
        columList.add(urlTxt.getText().trim());
        columList.add(driverClassName);

        dataSourceTableModel.addRow(columList.toArray(new Object[]{columList.size()}));
        dataSourceTable.updateUI();
    }

    /**
     * 删除一条数据源信息
     */
    private void removeDataSource(ActionEvent e) {
        if (this.dataSourceTable.getSelectedRow() < 0){
            Messages.showMessageDialog("请选择要删除的数据源所在的行","Error", Messages.getErrorIcon());
            return;
        }

        DefaultTableModel dataSourceTableModel = (DefaultTableModel)dataSourceTable.getModel();

        dataSourceTableModel.removeRow(this.dataSourceTable.getSelectedRow());
        dataSourceTable.updateUI();
    }

    /**
     * 测试数据源连接是否正常
     */
    private void testConnect(ActionEvent e) {
        if (this.dataSourceTable.getSelectedRow() < 0){
            Messages.showMessageDialog("请选择要删除的数据源所在的行","Error", Messages.getErrorIcon());
            return;
        }

        DefaultTableModel dataSourceTableModel = (DefaultTableModel)dataSourceTable.getModel();
        String aliasName = (String) dataSourceTableModel.getValueAt(this.dataSourceTable.getSelectedRow(), DataSourceColumnEnum.AliasName.getOrder());
        boolean connectOk = TableFactory.getInstance().testConnection(aliasName);
        if (connectOk){
            Messages.showMessageDialog("数据源连接正常","连接状态", Messages.getInformationIcon());
        }else{
            Messages.showMessageDialog("数据源连接异常","连接状态", Messages.getInformationIcon());
        }
    }

    /**
     * 初始化模板配置信息
     */
    private void initTemplateList(Map<String, CodeTemplateModel> templateMap) {
        java.util.List<CodeTemplateModel> list2 = new ArrayList<CodeTemplateModel>();

        if (templateMap != null && !templateMap.isEmpty()){
            for (Map.Entry<String, CodeTemplateModel> entry : templateMap.entrySet()) {
                list2.add(entry.getValue());
            }
        }

        templateList = new JBList<CodeTemplateModel>(list2);
        // 单选
        templateList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // 添加选项选中状态被改变的监听器
        templateList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                CodeTemplateModel model = (CodeTemplateModel)templateList.getSelectedValue();
                templateTypeTxt.setText(model == null ? "" : model.getType());
                templateContentTxtArea.setText(model == null ? "" : model.getContent().trim());
            }
        });

        // 设置默认选中项
        templateList.setSelectedIndex(0);

        // 添加到内容面板容器
        templateScrollPanel.getViewport().add(templateList);

        // 设置关联显示模板内容的textArea不容许编辑
        templateContentTxtArea.setEditable(false);
    }

    /**
     * 展示模板添加子页面
     */
    private void addTemplateGui(ActionEvent e) {
        TemplateAddGui dialog = new TemplateAddGui(this);
        dialog.setModal(true);
        dialog.pack();
        dialog.setVisible(true);
    }

    /**
     * 编辑一条模板信息
     */
    private void editTemplateGui(ActionEvent e) {
        if (this.templateList.getSelectedIndex() < 0){
            Messages.showMessageDialog("请选择要编辑的模板","Error", Messages.getErrorIcon());
            return;
        }

        DefaultListModel listModel = (DefaultListModel )templateList.getModel();
        CodeTemplateModel model = (CodeTemplateModel)templateList.getSelectedValue();

        TemplateEditGui dialog = new TemplateEditGui(this, model);
        dialog.setModal(true);
        dialog.pack();
        dialog.setVisible(true);
    }

    /**
     * 删除一条模板信息
     */
    private void removeTemplate(ActionEvent e) {
        if (this.templateList.getSelectedIndex() < 0){
            Messages.showMessageDialog("请选择要删除的模板","Error", Messages.getErrorIcon());
            return;
        }

        DefaultListModel listModel = (DefaultListModel )templateList.getModel();
        listModel.remove(templateList.getSelectedIndex());
    }

    /**
     * 从子界面添加一条模板配置信息到List
     */
    public void addTemplateToList(CodeTemplateModel model) {
        DefaultListModel listModel = (DefaultListModel )templateList.getModel();
        listModel.add(listModel.getSize(), model);
        templateList.setSelectedIndex(0);
    }

    /**
     * 从子界面编辑一条模板配置信息到List
     */
    public void editTemplateToList(CodeTemplateModel editModel) {
        String editTempalteName = editModel.getTemplateName();

        Map<String, CodeTemplateModel> templateMap = new HashMap<>();

        ListModel<CodeTemplateModel> tm = templateList.getModel();
        int count = tm.getSize();

        for(int i=0;i<count;i++){
            CodeTemplateModel model = tm.getElementAt(i);
            String templateName = model.getTemplateName().trim();

            if (editTempalteName.equalsIgnoreCase(templateName)){
                model.setContent(editModel.getContent().trim());
                this.codeTemplateSettings.removeTemplate(templateName);
            }

            templateMap.put(templateName, model);
        }

        initTemplateList(templateMap);
    }

    public void refresh(DataSourceSettings settings, CodeTemplateSettings codeTemplateSettings, GeneratorDefaultSettings generatorDefaultSettings) {
        initDataSourceTable(settings.getDataSourceMap());
        initTemplateList(codeTemplateSettings.getTemplateMap());
        initCustomerConfig();
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JBTable getDataSourceTable() {
        return dataSourceTable;
    }

    public Map<String, DataSourceModel> getDataSourceMap() {
        Map<String, DataSourceModel> dataSourceModelMap = new HashMap<>();
        TableModel tm = dataSourceTable.getModel();
        int count = tm.getRowCount();
        for(int i=0;i<count;i++){
            DataSourceModel model = new DataSourceModel();
            String aliasName = String.valueOf(tm.getValueAt(i, DataSourceColumnEnum.AliasName.getOrder())).trim();
            model.setAliasName(aliasName);
            model.setHostIp(String.valueOf(tm.getValueAt(i, DataSourceColumnEnum.Host.getOrder())).trim());
            model.setDbName(String.valueOf(tm.getValueAt(i, DataSourceColumnEnum.DataBase.getOrder())).trim());
            model.setUserName(String.valueOf(tm.getValueAt(i, DataSourceColumnEnum.User.getOrder())).trim());
            model.setPassword(String.valueOf(tm.getValueAt(i, DataSourceColumnEnum.Password.getOrder())).trim());
            model.setUrl(String.valueOf(tm.getValueAt(i, DataSourceColumnEnum.Url.getOrder())).trim());
            model.setDriverClassName(String.valueOf(tm.getValueAt(i, DataSourceColumnEnum.Driver.getOrder())).trim());
            dataSourceModelMap.put(aliasName, model);
        }

        return dataSourceModelMap;
    }

    public JBList getTemplateList() {
        return templateList;
    }

    public int getTemplateListItemCount() {
        if (templateList == null) {
            return 0;
        }

        return templateList.getItemsCount();
    }

    public Map<String, CodeTemplateModel> getTemplateMap() {
        Map<String, CodeTemplateModel> templateMap = new HashMap<>();
        ListModel<CodeTemplateModel> tm = templateList.getModel();
        int count = tm.getSize();

        for(int i=0;i<count;i++){
            CodeTemplateModel model = tm.getElementAt(i);
            String templateName = model.getTemplateName().trim();

            CodeTemplateModel temp = new CodeTemplateModel();
            temp.setTemplateName(model.getTemplateName().trim());
            temp.setContent(model.getContent().trim());
            temp.setType(model.getType().trim());

            templateMap.put(templateName, temp);
        }

        return templateMap;
    }

    /**
     * 模板配置可选参数panel
     */
    private void initSampleTreeTable() {
        sampleTreeTable = new JXTreeTable(new SampleTreeTableModel(SampleDemoGenerator.createSampleData()));
        sampleTreeTable.getColumnModel().getColumn(SampleEnum.Name.getOrder()).setPreferredWidth(110);
        sampleTreeTable.getColumnModel().getColumn(SampleEnum.ParamName.getOrder()).setPreferredWidth(30);
        sampleTreeTable.getColumnModel().getColumn(SampleEnum.ParamType.getOrder()).setPreferredWidth(20);
        sampleTreeTable.getColumnModel().getColumn(SampleEnum.ParamDefault.getOrder()).setPreferredWidth(20);
        sampleTreeTable.getColumnModel().getColumn(SampleEnum.ParamExplain.getOrder()).setPreferredWidth(350);
        sampleTreeTable.setRootVisible(true);
        sampleTreeTable.expandAll();

        templateParamScrollPanel.getViewport().add(sampleTreeTable);
    }

    /**
     * 初始化用户自定义配置
     */
    private void initCustomerConfig() {
        // 获取默认配置
        GeneratorDefaultModel generatorDefaultModel = SettingFacade.getGeneratorDefault();

        // 用户自定义参数加载进table
        Map<String, String> customerConfigMap = generatorDefaultModel.getCustomerConfigMap();

        int num = 0;
        java.util.List<Object[]> list2 = null;

        if (customerConfigMap != null && !customerConfigMap.isEmpty()) {
            list2 = new ArrayList<Object[]>();
            for (Map.Entry<String, String> entry : customerConfigMap.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();

                java.util.List<Object> list1 = new ArrayList<Object>();
                num++;
                list1.add(num);
                list1.add(key);
                list1.add(value);
                list2.add(list1.toArray(new Object[]{list1.size()}));
            }
        }
        Object[][] data = null;
        int height = 5;
        if (list2 != null && !list2.isEmpty()){
            height = list2.size();
            data = list2.toArray(new Object[][]{new Object[]{list2.size()}});
        }
        customerConfigScrollPanel.setSize((int) customerConfigScrollPanel.getSize().getWidth(), (int) customerConfigScrollPanel.getSize().getHeight() * height / 4);

        TableModel model = new DefaultTableModel(data, customerConfigColNames){
            private static final long serialVersionUID = -8310812824505049103L;

            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false;
            }
        };
        customerConfigTable = new JBTable(model);
        customerConfigTable.setBackground(background);
        customerConfigTable.getTableHeader().setReorderingAllowed(false);

        //No.列尝试不同的前景色
        TableColumn tableColumn = customerConfigTable.getColumn(CustomerConfigColumnEnum.No.getName());
        DefaultTableCellRenderer cellRender = new DefaultTableCellRenderer();
        cellRender.setHorizontalAlignment(SwingConstants.CENTER);
        tableColumn.setCellRenderer(cellRender);
        tableColumn.setPreferredWidth(30);

        customerConfigScrollPanel.getViewport().add(customerConfigTable);

    }

    /**
     * 添加用户自定义配置项
     */
    private void addCustomerConfig(ActionEvent e) {
        if (StringUtil.isEmpty(configKeyTxt.getText()) || StringUtil.isEmpty(configValueTxt.getText())){
            Messages.showMessageDialog("添加的自定义参数名和参数值不能为空","Error", Messages.getErrorIcon());
            return;
        }

        String configKey = configKeyTxt.getText().trim();
        Map<String, String> customerConfigMap = SettingFacade.getGeneratorDefault().getCustomerConfigMap();
        if (customerConfigMap != null && customerConfigMap.get(configKey) != null){
            Messages.showMessageDialog("新添加的自定义参数名已经存在","Error", Messages.getErrorIcon());
            return;
        }

        int rowCount = customerConfigTable.getRowCount();
        DefaultTableModel customerConfigTableModel = (DefaultTableModel)customerConfigTable.getModel();

        java.util.List<String> columList = new ArrayList<String>();
        columList.add(String.valueOf(rowCount+1));
        columList.add(configKey);
        columList.add(configValueTxt.getText().trim());

        customerConfigTableModel.addRow(columList.toArray(new Object[]{columList.size()}));
        customerConfigTable.updateUI();

        configKeyTxt.setText("");
        configValueTxt.setText("");
    }

    /**
     * 移除用户自定义配置项
     */
    private void removeCustomerConfig(ActionEvent e) {
        if (this.customerConfigTable.getSelectedRow() < 0){
            Messages.showMessageDialog("请选择要删除的自定义配置项所在的行","Error", Messages.getErrorIcon());
            return;
        }

        DefaultTableModel customerConfigTableModel = (DefaultTableModel)customerConfigTable.getModel();

        customerConfigTableModel.removeRow(this.customerConfigTable.getSelectedRow());
        customerConfigTable.updateUI();
    }

    /**
     * 用户自定义设置
     */
    private void applyCustomerConfig() {
        GeneratorDefaultModel defaultModel = SettingFacade.getGeneratorDefault();
        Map<String, String> customerConfig = new HashMap<String, String>();

        // 获取自定义配置
        DefaultTableModel tm = (DefaultTableModel)customerConfigTable.getModel();
        int count = tm.getRowCount();

        for(int i=0;i<count;i++){
            String configKey = String.valueOf(tm.getValueAt(i, CustomerConfigColumnEnum.Key.getOrder())).trim();
            String configValue = String.valueOf(tm.getValueAt(i, CustomerConfigColumnEnum.Value.getOrder())).trim();
            customerConfig.put(configKey, configValue);
        }

        defaultModel.setCustomerConfigMap(customerConfig);

        // 更新默认配置
        SettingFacade.getGeneratorDefaultSettings().setGeneratorDefaultModel(defaultModel);
    }

    /**
     * 获取通用配置
     */
    public Map<String, String> getCustomerConfigMap() {
        Map<String, String> customerConfig = new HashMap<String, String>();

        // 获取自定义配置
        DefaultTableModel tm = (DefaultTableModel)customerConfigTable.getModel();
        int count = tm.getRowCount();

        for(int i=0;i<count;i++){
            String configKey = String.valueOf(tm.getValueAt(i, CustomerConfigColumnEnum.Key.getOrder())).trim();
            String configValue = String.valueOf(tm.getValueAt(i, CustomerConfigColumnEnum.Value.getOrder())).trim();
            customerConfig.put(configKey, configValue);
        }

        return customerConfig;
    }

}
