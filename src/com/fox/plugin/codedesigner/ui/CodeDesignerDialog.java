package com.fox.plugin.codedesigner.ui;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.fox.plugin.codedesigner.db.setting.constant.TableEnum;
import com.fox.plugin.codedesigner.db.table.TableFactory;
import com.fox.plugin.codedesigner.db.table.model.Table;
import com.fox.plugin.codedesigner.generator.helper.GeneratorHelper;
import com.fox.plugin.codedesigner.generator.model.GeneratorModel;
import com.fox.plugin.codedesigner.setting.CodeTemplateSettings;
import com.fox.plugin.codedesigner.setting.DataSourceSettings;
import com.fox.plugin.codedesigner.setting.SettingFacade;
import com.fox.plugin.codedesigner.setting.constant.CustomerConfigColumnEnum;
import com.fox.plugin.codedesigner.setting.model.CodeTemplateModel;
import com.fox.plugin.codedesigner.setting.model.DataSourceModel;
import com.fox.plugin.codedesigner.setting.model.GeneratorDefaultModel;
import com.fox.plugin.codedesigner.util.EnvUtil;
import com.fox.plugin.codedesigner.util.StringUtil;
import com.intellij.openapi.editor.colors.EditorColorsManager;
import com.intellij.openapi.editor.colors.EditorColorsScheme;
import com.intellij.openapi.fileChooser.FileChooser;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.components.JBList;
import com.intellij.ui.table.JBTable;

/**
 * CodeDesignerDialog<br>
 *
 * @author fox
 */
public class CodeDesignerDialog extends JDialog {
    private static final long serialVersionUID = -6852021935942625714L;

    private JPanel mainPanel;
    private JTabbedPane codeTabbedPane1;
    private JPanel dataBasePanel;
    private JPanel templateConfigPanel;
    private JPanel dataSourcePanel;
    private JPanel tablesPanel;
    private JComboBox dataSourceComboBox;
    private JButton connectBtn;
    private JLabel datasourceLbl;
    private JScrollPane tablesScrollPanel;
    private JButton tablesSaveBtn;
    private JButton columnsEditBtn;
    private JButton showNext;
    private JPanel operationPanel;
    private JPanel defaultConfigPanel;
    private JTextField tableRemovePrefixTxt;
    private JTextField authorTxt;
    private JLabel tableRemovePrefixLbl;
    private JLabel authorLbl;
    private JLabel tableRemovePrefixTipLbl;
    private JPanel customerConfigPanel;
    private JScrollPane customerConfigScrollPanel;
    private JButton applyBtn;
    private JButton configDeleteBtn;
    private JButton configAddBtn;
    private JTextField configKeyTxt;
    private JTextField configValueTxt;
    private JLabel configKeyLbl;
    private JLabel configValueLbl;
    private JPanel templateSelectPanel;
    private JButton genreatorBtn;
    private JScrollPane templateScrollPanel;
    private JLabel templateTypeLbl;
    private JTextField templateTypeTxt;
    private JScrollPane templateContentScrollPanel;
    private JTextArea templateContentTxtArea;
    private JSplitPane templateSplitPanel;
    private JTextField outPutDirTxt;
    private JButton outPutDirBtn;
    private JLabel outPutDirLbl;

    /**
     * 数据源配置
     */
    private DataSourceSettings dataSourceSettings;

    /**
     * 模板配置
     */
    private CodeTemplateSettings codeTemplateSettings;

    /**
     * 样式
     */
    EditorColorsScheme colorScheme;
    Color background;

    private final FileChooserDescriptor chooseFolderOnlyDescriptor = FileChooserDescriptorFactory.createSingleFolderDescriptor();

    /**
     * 数据库表table
     */
    JBTable dbTable;

    /**
     * 数据库表table-表头信息
     */
    private static String[] dbTableColums = TableEnum.getTableEnums();

    /**
     * 数据库字段table
     */
    JBTable dbColumnTable;


    /**
     * 自定义配置-table
     */
    JBTable customerConfigTable;

    /**
     * 自定义配置-表头信息
     */
    private static String[] customerConfigColNames = CustomerConfigColumnEnum.getCustomerConfigColumns();

    /**
     * 连接数据库后所有的表数据，以数据库表名做主键
     */
    private Map<String, Table> connectDBTableMap;

    /**
     * 模板展示table页数据加载List
     */
    JBList templateList;

    public CodeDesignerDialog(DataSourceSettings dataSourceSettings, CodeTemplateSettings codeTemplateSettings) {
        this.dataSourceSettings = dataSourceSettings;
        this.codeTemplateSettings = codeTemplateSettings;

        /*  dialog样式等设置 */
        colorScheme = EditorColorsManager.getInstance().getGlobalScheme();
        background = colorScheme.getDefaultBackground();
        this.setContentPane(mainPanel);
        this.setTitle("代码生成器");
        this.setModal(true);
        this.setBackground(background);
        this.setSize(850, 650);
        this.setLocationRelativeTo(null);

        // 初始化用户自定义配置
        initCustomerConfig();

        // 初始化数据源下拉列表
        initDbConnect();

        // 初始化模板table页数据
        initTemplateTableList();

        // 生成按钮监听
        genreatorBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                generatorCode(e);
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        mainPanel.registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

    }

    /**
     * 初始化用户自定义配置
     */
    private void initCustomerConfig() {
        // 获取默认配置
        GeneratorDefaultModel generatorDefaultModel = SettingFacade.getGeneratorDefault();
        tableRemovePrefixTxt.setText(generatorDefaultModel.getTableRemovePrefix());
        authorTxt.setText(generatorDefaultModel.getAuthor());
        outPutDirTxt.setText(StringUtil.isEmpty(generatorDefaultModel.getTemplateOutPutDir()) ? EnvUtil.project.getBasePath() : generatorDefaultModel.getTemplateOutPutDir());

        // 用户自定义参数加载进table
        Map<String, String> customerConfigMap = generatorDefaultModel.getCustomerConfigMap();

        int num = 0;
        java.util.List<Object[]> list2 = null;

        if (customerConfigMap != null && !customerConfigMap.isEmpty()) {
            list2 = new ArrayList<Object[]>();
            for (Map.Entry<String, String> entry : customerConfigMap.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();

                java.util.List<Object> list1 = new ArrayList<Object>();
                num++;
                list1.add(num);
                list1.add(key);
                list1.add(value);
                list2.add(list1.toArray(new Object[]{list1.size()}));
            }
        }
        Object[][] data = null;
        int height = 5;
        if (list2 != null && !list2.isEmpty()){
            height = list2.size();
            data = list2.toArray(new Object[][]{new Object[]{list2.size()}});
        }
        customerConfigScrollPanel.setSize((int) customerConfigScrollPanel.getSize().getWidth(), (int) customerConfigScrollPanel.getSize().getHeight() * height / 4);

        TableModel model = new DefaultTableModel(data, customerConfigColNames){
            private static final long serialVersionUID = -8310812824505049103L;

            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false;
            }
        };
        customerConfigTable = new JBTable(model);
        customerConfigTable.setBackground(background);
        customerConfigTable.getTableHeader().setReorderingAllowed(false);

        //No.列尝试不同的前景色
        TableColumn tableColumn = customerConfigTable.getColumn(CustomerConfigColumnEnum.No.getName());
        DefaultTableCellRenderer cellRender = new DefaultTableCellRenderer();
        cellRender.setHorizontalAlignment(SwingConstants.CENTER);
        tableColumn.setCellRenderer(cellRender);
        tableColumn.setPreferredWidth(30);

        customerConfigScrollPanel.getViewport().add(customerConfigTable);

        // 添加路径选择监听
        outPutDirBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addTemplateOutPutDir(e);
            }
        });

        // 添加自定义配置监听
        configAddBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addCustomerConfig(e);
            }
        });

        // 删除自定义配置监听
        configDeleteBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeCustomerConfig(e);
            }
        });

        // 设置
        applyBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                applyCustomerConfig(e);
            }
        });
    }

    /**
     * 添加用户自定义配置项
     */
    private void addCustomerConfig(ActionEvent e) {
        if (StringUtil.isEmpty(configKeyTxt.getText()) || StringUtil.isEmpty(configValueTxt.getText())){
            Messages.showMessageDialog("添加的自定义参数名和参数值不能为空","Error", Messages.getErrorIcon());
            return;
        }

        String configKey = configKeyTxt.getText().trim();
        Map<String, String> customerConfigMap = SettingFacade.getGeneratorDefault().getCustomerConfigMap();
        if (customerConfigMap != null && customerConfigMap.get(configKey) != null){
            Messages.showMessageDialog("新添加的自定义参数名已经存在","Error", Messages.getErrorIcon());
            return;
        }

        int rowCount = customerConfigTable.getRowCount();
        DefaultTableModel customerConfigTableModel = (DefaultTableModel)customerConfigTable.getModel();

        java.util.List<String> columList = new ArrayList<String>();
        columList.add(String.valueOf(rowCount+1));
        columList.add(configKey);
        columList.add(configValueTxt.getText().trim());

        customerConfigTableModel.addRow(columList.toArray(new Object[]{columList.size()}));
        customerConfigTable.updateUI();

        configKeyTxt.setText("");
        configValueTxt.setText("");
    }

    /**
     * 移除用户自定义配置项
     */
    private void removeCustomerConfig(ActionEvent e) {
        if (this.customerConfigTable.getSelectedRow() < 0){
            Messages.showMessageDialog("请选择要删除的自定义配置项所在的行","Error", Messages.getErrorIcon());
            return;
        }

        DefaultTableModel customerConfigTableModel = (DefaultTableModel)customerConfigTable.getModel();

        customerConfigTableModel.removeRow(this.customerConfigTable.getSelectedRow());
        customerConfigTable.updateUI();
    }

    /**
     * 添加路径选择监听
     */
    private void addTemplateOutPutDir(ActionEvent e) {
        chooseFolderOnlyDescriptor.setTitle("选择路径");
        chooseFolderOnlyDescriptor.setDescription("选择代码生成输出根目录");
        VirtualFile file = FileChooser.chooseFile(chooseFolderOnlyDescriptor, EnvUtil.project, null);
        if (file != null) {
            outPutDirTxt.setText(file.getPath());
        }
    }

    /**
     * 用户自定义设置监听
     */
    private void applyCustomerConfig(ActionEvent e) {
        String templateOutPutDir = outPutDirTxt.getText();

        if (StringUtil.isEmpty(templateOutPutDir)){
            Messages.showMessageDialog("请选择代码生成路径","Error", Messages.getErrorIcon());
            return;
        }

        GeneratorDefaultModel defaultModel = new GeneratorDefaultModel();
        Map<String, String> customerConfig = new HashMap<String, String>();

        defaultModel.setTableRemovePrefix(StringUtil.isEmpty(tableRemovePrefixTxt.getText()) ? null : tableRemovePrefixTxt.getText().trim());
        defaultModel.setAuthor(StringUtil.isEmpty(authorTxt.getText()) ? null : authorTxt.getText().trim());
        defaultModel.setTemplateOutPutDir(templateOutPutDir.trim());

        // 获取自定义配置
        DefaultTableModel tm = (DefaultTableModel)customerConfigTable.getModel();
        int count = tm.getRowCount();

        for(int i=0;i<count;i++){
            String configKey = String.valueOf(tm.getValueAt(i, CustomerConfigColumnEnum.Key.getOrder())).trim();
            String configValue = String.valueOf(tm.getValueAt(i, CustomerConfigColumnEnum.Value.getOrder())).trim();
            customerConfig.put(configKey, configValue);
        }

        defaultModel.setCustomerConfigMap(customerConfig);

        // 更新默认配置
        SettingFacade.getGeneratorDefaultSettings().setGeneratorDefaultModel(defaultModel);

        // 切换面板显示数据库表panel
        codeTabbedPane1.setSelectedIndex(1);
    }

    /**
     * 初始化数据源下拉列表
     */
    private void initDbConnect() {
        Map<String, DataSourceModel> dataSourceMap = this.dataSourceSettings.getDataSourceMap();

        // 数据库连接下拉框
        for (Map.Entry<String, DataSourceModel> entry : dataSourceMap.entrySet()){
            dataSourceComboBox.addItem(entry.getValue());
        }

        // 数据库连接按钮监听
        connectBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connectDb(e);
            }
        });

        // 保存表设置
        tablesSaveBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveTableConfig(e);
            }
        });

        // 查看表字段按钮监听
        columnsEditBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showColumnEditDialog(e);
            }
        });

        // 保存表设置
        tablesSaveBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveTableConfig(e);
            }
        });

        // 下一步到模板选择页
        showNext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showGeneratorPanel(e);
            }
        });
    }

    /**
     * 数据库连接按钮监听
     */
    private void connectDb(ActionEvent e) {
        DataSourceModel dbSelected = (DataSourceModel)dataSourceComboBox.getSelectedItem();

        // 获取解析后数据库所有表和字段数据
        java.util.List<Table> tables = TableFactory.getInstance().getAllTables(dbSelected.getAliasName());
        initDbTableMap(tables);

        int num = 0;
        java.util.List<Object[]> list2 = null;

        if (tables != null && !tables.isEmpty()) {
            list2 = new ArrayList<Object[]>();
            for (Table table : tables) {
                java.util.List<Object> list1 = new ArrayList<Object>();
                num++;
                list1.add(Boolean.FALSE);
                list1.add(num);
                list1.add(table.getSqlName());
                list1.add(table.getClassName());
                list1.add(table.getClassNameLower());
                list1.add(table.getRemarks());

                list2.add(list1.toArray(new Object[]{list1.size()}));
            }
        }
        Object[][] data = null;
        int height = 5;
        if (list2 != null && !list2.isEmpty()){
            height = list2.size();
            data = list2.toArray(new Object[][]{new Object[]{list2.size()}});
        }
        tablesScrollPanel.setSize((int) customerConfigScrollPanel.getSize().getWidth(), (int) tablesScrollPanel.getSize().getHeight() * height / 4);

        TableModel model = new DefaultTableModel(data, dbTableColums){
            private static final long serialVersionUID = -8310812824505049103L;

            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) {
                //相应的列不可编辑
                if (colIndex == 1 || colIndex == 2) {
                    return false;
                }
                return true;
            }
        };
        dbTable = new JBTable(model);
        dbTable.setBackground(background);
        dbTable.getTableHeader().setReorderingAllowed(false);

        //No.列尝试不同的前景色
        TableColumn tableColumn = dbTable.getColumn(TableEnum.No.getName());
        DefaultTableCellRenderer cellRender = new DefaultTableCellRenderer();
        cellRender.setHorizontalAlignment(SwingConstants.CENTER);
        tableColumn.setCellRenderer(cellRender);
        tableColumn.setPreferredWidth(50);

        //Selected列是checkbox
        TableColumn tableColumnDO = dbTable.getColumn(TableEnum.Selected.getName());
        //tableColumnDO.setCellEditor(new CheckBoxCellEditor());
        //tableColumnDO.setCellRenderer(new CheckBoxRenderer());
        tableColumnDO.setCellEditor(dbTable.getDefaultEditor(Boolean.class));
        tableColumnDO.setCellRenderer(dbTable.getDefaultRenderer(Boolean.class));
        tableColumnDO.setPreferredWidth(50);

        TableColumn tableColumnSql = dbTable.getColumn(TableEnum.SqlName.getName());
        tableColumnSql.setPreferredWidth(200);

        TableColumn tableColumnClassName = dbTable.getColumn(TableEnum.ClassName.getName());
        tableColumnClassName.setPreferredWidth(200);

        TableColumn tableColumnClassNameLower = dbTable.getColumn(TableEnum.ClassNameLower.getName());
        tableColumnClassNameLower.setPreferredWidth(200);

        TableColumn tableColumnRemarks = dbTable.getColumn(TableEnum.Remarks.getName());
        tableColumnRemarks.setPreferredWidth(200);

        tablesScrollPanel.getViewport().add(dbTable);
    }

    /**
     * 构建用户此次连接获取的表信息
     */
    private void initDbTableMap(List<Table> tables) {
        if (tables == null || tables.isEmpty()){
            return;
        }
        Map<String,Table> connectDBTableMap = new HashMap<String, Table>();

        for (Table table : tables){
            connectDBTableMap.put(table.getSqlName().trim(), table);
        }

        this.connectDBTableMap = connectDBTableMap;
    }

    /**
     * 展示表字段编辑界面
     */
    private void showColumnEditDialog(ActionEvent e) {
        if (this.dbTable == null){
            Messages.showMessageDialog("请先连接数据库","Error", Messages.getErrorIcon());
            return;
        }

        if (this.dbTable.getSelectedRow() < 0){
            Messages.showMessageDialog("请选择要查看的表所在行","Error", Messages.getErrorIcon());
            return;
        }

        // 获取所选择行的表信息
        int row = this.dbTable.getSelectedRow();
        String sqlName = String.valueOf(this.dbTable.getModel().getValueAt(row, TableEnum.SqlName.getOrder())).trim();
        Table currentTable = this.connectDBTableMap.get(sqlName);

        ColumnEditGui dialog = new ColumnEditGui(this, currentTable);
        dialog.setModal(true);
        dialog.pack();
        dialog.setVisible(true);
    }

    /**
     * 保存表设置
     */
    private void saveTableConfig(ActionEvent e) {
        if (this.dbTable == null){
            Messages.showMessageDialog("请先连接数据库","Error", Messages.getErrorIcon());
            return;
        }

        // 获取表model
        DefaultTableModel tm = (DefaultTableModel)dbTable.getModel();
        int count = tm.getRowCount();

        // 没有数据就return
        if (count < 0){
            return;
        }

        // 这里我是处理成所有表都容许修改，最后可以单选框选择要生成代码的表
        for(int i=0;i<count;i++){
            String sqlName = String.valueOf(tm.getValueAt(i, TableEnum.SqlName.getOrder())).trim();
            Table currentTable = this.connectDBTableMap.get(sqlName);

            currentTable.setClassName(String.valueOf(tm.getValueAt(i, TableEnum.ClassName.getOrder())).trim());
            currentTable.setClassNameLower(String.valueOf(tm.getValueAt(i, TableEnum.ClassNameLower.getOrder())).trim());
            currentTable.setRemarks(String.valueOf(tm.getValueAt(i, TableEnum.Remarks.getOrder())).trim());
        }
    }

    /**
     * 下一步到模板选择页
     */
    private void showGeneratorPanel(ActionEvent e) {
        if (this.dbTable == null){
            Messages.showMessageDialog("请先连接数据库","Error", Messages.getErrorIcon());
            return;
        }

        // 获取表model
        DefaultTableModel tm = (DefaultTableModel)dbTable.getModel();
        int count = tm.getRowCount();

        // 没有数据就return
        if (count < 0){
            return;
        }

        // 这里处理成所有表都容许修改，最后可以单选框选择要生成代码的表
        Boolean selected = Boolean.FALSE;
        for(int i=0;i<count;i++){
            Boolean choosed = (Boolean) tm.getValueAt(i, TableEnum.Selected.getOrder());
            if (choosed){
                selected = true;
                break;
            }
        }

        if (!selected){
            Messages.showMessageDialog("请勾选要生成代码的表所在行的选择框","Error", Messages.getErrorIcon());
            return;
        }

        // 切换面板显示数据库表panel
        codeTabbedPane1.setSelectedIndex(2);
    }

    /**
     * 初始化模板table页数据
     */
    private void initTemplateTableList() {
        // 获取所有配置模板
        Map<String, CodeTemplateModel> templateMap = SettingFacade.getAllTemplate();

        java.util.List<CodeTemplateModel> list2 = new ArrayList<CodeTemplateModel>();
        if (templateMap != null && !templateMap.isEmpty()){
            for (Map.Entry<String, CodeTemplateModel> entry : templateMap.entrySet()) {
                list2.add(entry.getValue());
            }
        }

        templateList = new JBList<CodeTemplateModel>(list2);
        // 模板选择list容许多选
        templateList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        // 添加选项选中状态被改变的监听器
        templateList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                CodeTemplateModel model = (CodeTemplateModel)templateList.getSelectedValue();
                templateTypeTxt.setText(model == null ? "" : model.getType());
                templateContentTxtArea.setText(model == null ? "" : model.getContent().trim());
            }
        });

        // 设置默认选中项
        templateList.setSelectedIndex(0);

        // 添加到内容面板容器
        templateScrollPanel.getViewport().add(templateList);

        // 设置关联显示模板内容的textArea不容许编辑
        templateContentTxtArea.setEnabled(false);
    }

    /**
     * 生成按钮监听
     */
    private void generatorCode(ActionEvent e) {
        // 获取表model
        DefaultTableModel tm = (DefaultTableModel)dbTable.getModel();
        int count = tm.getRowCount();

        // 没有数据就return
        if (count < 0){
            Messages.showMessageDialog("没有可处理的表数据","Error", Messages.getErrorIcon());
            codeTabbedPane1.setSelectedIndex(1);
            return;
        }

        // 这里处理成所有表都容许修改，最后可以单选框选择要生成代码的表
        List<Table> tableList = new LinkedList<Table>();
        Boolean selected = Boolean.FALSE;
        for(int i=0;i<count;i++){
            Boolean choosed = (Boolean) tm.getValueAt(i, TableEnum.Selected.getOrder());
            if (choosed){
                selected = true;
                String sqlName = String.valueOf(tm.getValueAt(i, TableEnum.SqlName.getOrder()));
                Table table = this.connectDBTableMap.get(sqlName);
                tableList.add(table);
            }
        }

        if (!selected){
            Messages.showMessageDialog("请勾选要生成代码的表所在行的选择框","Error", Messages.getErrorIcon());
            // 切换面板显示数据库表panel
            codeTabbedPane1.setSelectedIndex(1);
            return;
        }

        // 收集模板选择
        List<String> templateNameList = new LinkedList<String>();
        // 获取所有被选中的选项索引
        int[] indices = templateList.getSelectedIndices();
        if (indices.length == 0){
            Messages.showMessageDialog("请选择要生成代码的模板","Error", Messages.getErrorIcon());
            codeTabbedPane1.setSelectedIndex(2);
            return;
        }

        // 获取选项数据的 ListModel
        ListModel<CodeTemplateModel> listModel = templateList.getModel();
        // 输出选中的选项
        for (int index : indices) {
            CodeTemplateModel model = (CodeTemplateModel)listModel.getElementAt(index);
            templateNameList.add(model.getTemplateName());
        }

        // 代码生成
        GeneratorModel generatorModel = new GeneratorModel();
        generatorModel.setTableList(tableList);
        generatorModel.setTemplateList(templateNameList);

        GeneratorHelper.generate(generatorModel);

        // 刷新项目
        EnvUtil.project.getBaseDir().refresh(false, true);
    }

    private void onCancel() {
        dispose();
    }
}
