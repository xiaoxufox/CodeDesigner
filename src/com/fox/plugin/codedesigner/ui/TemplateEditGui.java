package com.fox.plugin.codedesigner.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.fox.plugin.codedesigner.setting.model.CodeTemplateModel;
import com.fox.plugin.codedesigner.util.StringUtil;
import com.intellij.openapi.editor.colors.EditorColorsManager;
import com.intellij.openapi.editor.colors.EditorColorsScheme;
import com.intellij.openapi.ui.Messages;

/**
 * TemplateEditGui<br>
 *
 * @author fox
 */
public class TemplateEditGui extends JDialog{

    private static final long serialVersionUID = 4557405604892804921L;

    private JPanel mainPanel;
    private JTextField templateNameTxt;
    private JLabel templateNameLbl;
    private JLabel templateContentLbl;
    private JButton addBtn;
    private JTextArea contentTextArea;
    private JLabel templateTypeLbl;
    private JTextField teplateTypeTxt;
    private JButton cancelBtn;

    private CodeDesignerConfigurationGui configurationGui;

    //样式
    EditorColorsScheme colorScheme;
    Color background;

    public TemplateEditGui(CodeDesignerConfigurationGui configurationGui, CodeTemplateModel model){
        colorScheme = EditorColorsManager.getInstance().getGlobalScheme();
        background = colorScheme.getDefaultBackground();
        this.configurationGui = configurationGui;
        this.setTitle("添加模板");
        this.setContentPane(mainPanel);
        this.setModal(true);
        this.setBackground(background);
        this.setSize(800, 600);
        this.setLocationRelativeTo(null);

        templateNameTxt.setText(model.getTemplateName());
        teplateTypeTxt.setText(model.getType());
        contentTextArea.setText(model.getContent());

        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addTemplate(e);
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        mainPanel.registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * 添加模板
     */
    private void addTemplate(ActionEvent e) {
        if (StringUtil.isEmpty(contentTextArea.getText())){
            Messages.showMessageDialog("模板内容不能为空","Error", Messages.getErrorIcon());
            return;
        }

        String templateName = templateNameTxt.getText().trim();
        String content = contentTextArea.getText().trim();
        String templateType = templateNameTxt.getText().trim();

        CodeTemplateModel model = new CodeTemplateModel(templateName, content, templateType);

        this.configurationGui.editTemplateToList(model);

        onCancel();
    }


    private void onCancel() {
        dispose();
    }

}
