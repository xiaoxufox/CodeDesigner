package com.fox.plugin.codedesigner.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;

import com.fox.plugin.codedesigner.setting.constant.TemplateTypeEnum;
import com.fox.plugin.codedesigner.setting.model.CodeTemplateModel;
import com.fox.plugin.codedesigner.util.StringUtil;
import com.intellij.openapi.editor.colors.EditorColorsManager;
import com.intellij.openapi.editor.colors.EditorColorsScheme;
import com.intellij.openapi.ui.Messages;

/**
 * TemplateAddGui<br>
 *
 * @author fox
 */
public class TemplateAddGui extends JDialog{

    private static final long serialVersionUID = 4557405604892804921L;

    private JPanel mainPanel;
    private JTextField templateNameTxt;
    private JLabel templateNameLbl;
    private JLabel templateContentLbl;
    private JButton addBtn;
    private JTextArea contentTextArea;
    private JComboBox templateTypeComboBox;
    private JLabel templateTypeLbl;
    private JButton cancelBtn;

    private CodeDesignerConfigurationGui configurationGui;

    //样式
    EditorColorsScheme colorScheme;
    Color background;

    public TemplateAddGui(CodeDesignerConfigurationGui configurationGui){
        colorScheme = EditorColorsManager.getInstance().getGlobalScheme();
        background = colorScheme.getDefaultBackground();
        this.configurationGui = configurationGui;
        this.setTitle("添加模板");
        this.setContentPane(mainPanel);
        this.setModal(true);
        this.setBackground(background);
        this.setSize(800, 600);
        this.setLocationRelativeTo(null);

        java.util.List<String> templateTypeList = TemplateTypeEnum.getTemplateTypeList();
        for (String type : templateTypeList){
            templateTypeComboBox.addItem(type);
        }

        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addTemplate(e);
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        mainPanel.registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * 添加模板
     */
    private void addTemplate(ActionEvent e) {
        if (StringUtil.isEmpty(templateNameTxt.getText()) || StringUtil.isEmpty(contentTextArea.getText())){
            Messages.showMessageDialog("模板名称和模板内容不能为空","Error", Messages.getErrorIcon());
            return;
        }

        String templateName = templateNameTxt.getText().trim();
        String content = contentTextArea.getText().trim();
        String templateType = (String)templateTypeComboBox.getSelectedItem();

        Map<String, CodeTemplateModel> templateModelMap = this.configurationGui.getTemplateMap();
        if (templateModelMap != null && templateModelMap.get(templateName) != null){
            Messages.showMessageDialog("该模板名的配置已经存在","Error", Messages.getErrorIcon());
            return;
        }

        CodeTemplateModel model = new CodeTemplateModel(templateName, content, templateType);

        this.configurationGui.addTemplateToList(model);

        onCancel();
    }


    private void onCancel() {
        dispose();
    }

}
