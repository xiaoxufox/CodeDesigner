package com.fox.plugin.codedesigner.ui.util;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import java.awt.*;

/**
 * CheckBoxCellEditor<br>
 * 废弃
 * @author copy from other
 */
public class CheckBoxCellEditor extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = -1617725820739126108L;

    protected JCheckBox checkBox;
    public CheckBoxCellEditor() {
        checkBox = new JCheckBox();
        checkBox.setHorizontalAlignment(SwingConstants.CENTER);
        // checkBox.setBackground( Color.white);
    }
    @Override public Object getCellEditorValue() {
        return checkBox.isSelected();
    }
    @Override public Component getTableCellEditorComponent(
            JTable  table,
            Object  value,
            boolean isSelected,
            int     row,
            int     column) {
        checkBox.setSelected((Boolean) value);
        return checkBox;
    }
}
