package com.fox.plugin.codedesigner.ui.util;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * CheckBoxRenderer<br>
 * 废弃
 *
 * @author copy from other
 */
public class CheckBoxRenderer extends JCheckBox implements TableCellRenderer {
    private static final long serialVersionUID = -2968692021453075877L;

    Border border = new EmptyBorder(1, 2, 1, 2);
    public CheckBoxRenderer() {
        super();
        setOpaque(true);
        setHorizontalAlignment(SwingConstants.CENTER);
    }

    @Override public Component getTableCellRendererComponent(JTable  table, Object  value, boolean isSelected,
                                                             boolean hasFocus, int row, int column) {
        if (value instanceof Boolean) {
            setSelected((Boolean) value);
             setEnabled(table.isCellEditable(row, column));
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }
        return this;
    }
}
