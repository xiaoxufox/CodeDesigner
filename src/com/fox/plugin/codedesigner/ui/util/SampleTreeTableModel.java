package com.fox.plugin.codedesigner.ui.util;

import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableNode;

import com.fox.plugin.codedesigner.setting.constant.SampleEnum;
import com.fox.plugin.codedesigner.setting.model.SampleModel;

/**
 * SampleTreeTableModel<br>
 * treeTable的model
 *
 * @author fox
 */
public class SampleTreeTableModel extends DefaultTreeTableModel {
    private String[] columnNames = SampleEnum.getSamples();
    private Class[] classesTypes = {String.class, String.class, String.class, String.class, String.class};

    public SampleTreeTableModel(TreeTableNode node) {
        super(node);
    }

    /**
     * 列的类型
     */
    @Override
    public Class getColumnClass(int col) {
        return classesTypes[col];
    }

    /**
     * 列的数量
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * 表头显示的内容
     */
    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    /**
     * 返回在单元格中显示的Object
     */
    @Override
    public Object getValueAt(Object node, int column) {
        Object value = null;
        if (node instanceof DefaultMutableTreeTableNode) {
            DefaultMutableTreeTableNode mutableNode = (DefaultMutableTreeTableNode) node;
            Object o = mutableNode.getUserObject();
            if (o != null && o instanceof SampleModel) {
                SampleModel bean = (SampleModel) o;
                switch (column) {
                    case 0:
                        value = bean.getName();
                        break;
                    case 1:
                        value = bean.getParamName();
                        break;
                    case 2:
                        value = bean.getParamType();
                        break;
                    case 3:
                        value = bean.getParamDefault();
                        break;
                    case 4:
                        value = bean.getParamExplain();
                        break;
                }
            }
        }
        return value;
    }

    /**
     * 设置所有单元格都不能编辑
     */
    @Override
    public boolean isCellEditable(Object node, int column) {
        return false;
    }

}
