package com.fox.plugin.codedesigner.ui.util;

import java.util.LinkedList;

import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;

import com.fox.plugin.codedesigner.setting.constant.TemplateTypeEnum;
import com.fox.plugin.codedesigner.setting.model.CodeTemplateModel;
import com.fox.plugin.codedesigner.setting.model.SampleModel;

/**
 * SampleDemoGenerator<br>
 * 生成SampleModel用于页面展示
 *
 * @author fox
 */
public class SampleDemoGenerator {

    public static DefaultMutableTreeTableNode createSampleData() {
        DefaultMutableTreeTableNode root = new DefaultMutableTreeTableNode(new SampleModel("根"));

        /* 共享参数 */
        SampleModel allModel = new SampleModel("路径参数", "*_dir", "String", "", "配置string类型参数,添加_dir用于路径解析");
        root.add(new DefaultMutableTreeTableNode(allModel));

        SampleModel authorModel = new SampleModel("代码编辑作者", "author", "String", "admin", "允许通过配置修改");
        root.add(new DefaultMutableTreeTableNode(authorModel));

        SampleModel outputDirModel = new SampleModel("代码输出路径", "outputDir", "String", null, "项目根路径 ,允许通过配置修改");
        root.add(new DefaultMutableTreeTableNode(outputDirModel));

        SampleModel envModel = new SampleModel("系统环境变量", "", "Map", null, "系统的环境变量,取值时注意,因为使用freemarker,参数key的.替换为_");
        root.add(new DefaultMutableTreeTableNode(envModel));

        SampleModel systemModel = new SampleModel("系统变量", "", "Map", null, "系统变量,取值时注意,因为使用freemarker,参数key的.替换为_");
        root.add(new DefaultMutableTreeTableNode(systemModel));

        SampleModel nowModel = new SampleModel("当前时间", "now", "Date", null, "当前时间");
        root.add(new DefaultMutableTreeTableNode(nowModel));

        /* 表结构 */
        DefaultMutableTreeTableNode tableNode = new DefaultMutableTreeTableNode(new SampleModel("表结构", "table", "table", null, "表结构"));

        SampleModel sqlNameModel = new SampleModel("表名", "sqlName", "String", null, "表名");
        tableNode.add(new DefaultMutableTreeTableNode(sqlNameModel));

        SampleModel remarksModel = new SampleModel("表名备注", "remarks", "String", null, "表名备注");
        tableNode.add(new DefaultMutableTreeTableNode(remarksModel));

        SampleModel classNameModel = new SampleModel("转换后类名", "className", "String", null, "转换后类名");
        tableNode.add(new DefaultMutableTreeTableNode(classNameModel));

        SampleModel classNameLowerModel = new SampleModel("类名首字母小写", "classNameLower", "String", null, "类名首字母小写");
        tableNode.add(new DefaultMutableTreeTableNode(classNameLowerModel));

        /* 表结构对应字段配置 */
        DefaultMutableTreeTableNode columnsModel = new DefaultMutableTreeTableNode(new SampleModel("表字段", "columns", "List", null, "表对应的所有字段列表"));

        SampleModel sqlNameColumnModel = new SampleModel("字段名", "sqlName", "String", null, "数据库字段名");
        columnsModel.add(new DefaultMutableTreeTableNode(sqlNameColumnModel));

        SampleModel sqlTypeColumnModel = new SampleModel("字段类型", "sqlType", "int", null, "对应java.sql.Types的值");
        columnsModel.add(new DefaultMutableTreeTableNode(sqlTypeColumnModel));

        SampleModel sqlTypeNameColumnModel = new SampleModel("字段类型名", "sqlTypeName", "String", null, "varchar");
        columnsModel.add(new DefaultMutableTreeTableNode(sqlTypeNameColumnModel));

        SampleModel pkColumnModel = new SampleModel("是否主键", "pk", "boolean", null, "该字段是否主键");
        columnsModel.add(new DefaultMutableTreeTableNode(pkColumnModel));

        SampleModel remarksColumnModel = new SampleModel("字段注释", "remarks", "String", null, "字段注释");
        columnsModel.add(new DefaultMutableTreeTableNode(remarksColumnModel));

        SampleModel columnNameColumnModel = new SampleModel("转换后字段名", "columnName", "String", null, "代码生成会使用该名称");
        columnsModel.add(new DefaultMutableTreeTableNode(columnNameColumnModel));

        SampleModel javaTypeColumnModel = new SampleModel("字段JAVA类型", "javaType", "String", null, "字段数据库类型对应的java类型");
        columnsModel.add(new DefaultMutableTreeTableNode(javaTypeColumnModel));

        SampleModel columnNameLowerColumnModel = new SampleModel("字段首字母小写", "columnNameLower", "String", null, "字段数据库类型对应的java类型");
        columnsModel.add(new DefaultMutableTreeTableNode(columnNameLowerColumnModel));

        tableNode.add(columnsModel);

        root.add(new DefaultMutableTreeTableNode(sqlNameModel));
        root.add(new DefaultMutableTreeTableNode(remarksModel));
        root.add(new DefaultMutableTreeTableNode(classNameModel));
        root.add(new DefaultMutableTreeTableNode(classNameLowerModel));
        root.add(tableNode);

        return root;
    }

    /**
     * 创建代码模板示例
     */
    public static java.util.List<CodeTemplateModel> createTemplateDemo() {
        java.util.List<CodeTemplateModel> templateModelList = new LinkedList<>();

        CodeTemplateModel javaDemo = new CodeTemplateModel();
        javaDemo.setType(TemplateTypeEnum.Java.getName());
        javaDemo.setTemplateName("${basepackage_dir}.bean.${className}.java");
        javaDemo.setContent("<#assign className = table.className>\n" +
                "<#assign classNameLower = className?uncap_first>\n" +
                "package ${basepackage}.bean;\n" +
                "\n" +
                "import javax.persistence.Column;\n" +
                "import javax.persistence.Entity;\n" +
                "import javax.persistence.GeneratedValue;\n" +
                "import javax.persistence.GenerationType;\n" +
                "import javax.persistence.Id;\n" +
                "\n" +
                "import org.apache.ibatis.type.Alias;\n" +
                "\n" +
                "/**\n" +
                " * ${className}\n" +
                " <#if table.remarks?exists && table.remarks != '' && table.remarks != 'null'>\n" +
                " * ${table.remarks}\n" +
                " </#if>\n" +
                " *\n" +
                " * @author:admin\n" +
                " */\n" +
                "@Alias(\"${classNameLower}\")\n" +
                "@Entity(name = \"${table.sqlName}\")\n" +
                "public class ${className} implements java.io.Serializable{\n" +
                "\n" +
                "\tprivate static final long serialVersionUID = 1L;\n" +
                "\n" +
                "\t<#list table.columns as column>\n" +
                "\t<#if column.remarks?exists && column.remarks != '' && column.remarks != 'null'>\n" +
                "\t/** ${column.remarks} */\n" +
                "\t</#if>\n" +
                "\tprivate ${column.javaType} ${column.columnNameLower};\n" +
                "\n" +
                "\t</#list>\n" +
                "\tpublic ${className}(){\n" +
                "\t}\n" +
                "\n" +
                "\t<#list table.columns as column>\n" +
                "\t<#if column.pk>\n" +
                "\t@Id\n" +
                "\t@GeneratedValue(strategy = GenerationType.AUTO)\n" +
                "\t</#if>\n" +
                "\t@Column(name = \"${column.sqlName}\")\n" +
                "\tpublic ${column.javaType} get${column.columnName}() {\n" +
                "\t\treturn this.${column.columnNameLower};\n" +
                "\t}\n" +
                "\n" +
                "\tpublic void set${column.columnName}(${column.javaType} ${column.columnNameLower}) {\n" +
                "\t\tthis.${column.columnNameLower} = ${column.columnNameLower};\n" +
                "\t}\n" +
                "\n" +
                "\t</#list>\n" +
                "\n" +
                "    @Override\n" +
                "    public String toString(){\n" +
                "        return \"${className}{\"+\n" +
                "        <#list table.columns as column>\n" +
                "            \"${column.columnNameLower}=\" + ${column.columnNameLower} + '\\'' +\n" +
                "        </#list>\n" +
                "             '}';\n" +
                "\t}\n" +
                "}");

        CodeTemplateModel sqlDemo = new CodeTemplateModel();
        sqlDemo.setType(TemplateTypeEnum.Xml.getName());
        sqlDemo.setTemplateName("sqlMap_${className}.xml");
        sqlDemo.setContent("<#assign className = table.className>   \n" +
                "<#assign classNameFirstLower = table.classNameLower>\n" +
                "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                "<sqlMap namespace=\"${basepackage}.bean.${classNameFirstLower}\">\n" +
                "\n" +
                "\t<!-- query-条件 -->\n" +
                "\t<sql id=\"query${className}\">\n" +
                "\t\t<![CDATA[\n" +
                "\t\tSELECT \n" +
                "\t\t\t<#list table.columns as column>\n" +
                "        \t${column.sqlName} AS ${column.columnNameLower}<#if column_has_next>,</#if>\n" +
                "\t\t\t</#list>\n" +
                "\t    FROM ${table.sqlName} \n" +
                "\t    WHERE \n" +
                "\t\t\t1 = 1\n" +
                "        \t<#list table.columns as column>\n" +
                "\t\t\t<#if column.isDateTimeColumn>\n" +
                "\t\t\t<#noparse><#if</#noparse> ${column.columnNameLower}Start<#noparse>?exists && </#noparse>${column.columnNameLower}Start <#noparse>!= '' && </#noparse>${column.columnNameLower}Start <#noparse>!= 'null'></#noparse>\n" +
                "\t\t\tAND ${column.sqlName} >= :${column.columnNameLower}Start \n" +
                "\t\t\t<#noparse></#if></#noparse>\n" +
                "\t\t\t<#noparse><#if</#noparse> ${column.columnNameLower}End<#noparse>?exists && </#noparse>${column.columnNameLower}End <#noparse>!= '' && </#noparse>${column.columnNameLower}End <#noparse>!= 'null'></#noparse>\n" +
                "\t\t\tAND ${column.sqlName} <= :${column.columnNameLower}End \n" +
                "\t\t\t<#noparse></#if></#noparse>\t\n" +
                "\t\t\t<#else>\n" +
                "\t\t\t<#noparse><#if</#noparse> ${column.columnNameLower}<#noparse>?exists && </#noparse>${column.columnNameLower} <#noparse>!= '' && </#noparse>${column.columnNameLower} <#noparse>!= 'null'></#noparse>\n" +
                "\t\t\tAND ${column.sqlName} = :${column.columnNameLower} \n" +
                "\t\t\t<#noparse></#if></#noparse>\n" +
                "\t\t\t</#if>\n" +
                "\t        </#list>   \n" +
                "\t    ]]>\n" +
                "\t</sql>  \n" +
                "</sqlMap>");

        templateModelList.add(javaDemo);
        templateModelList.add(sqlDemo);


        return templateModelList;
    }
}
