package com.fox.plugin.codedesigner.db.setting.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * TableEnum<br>
 * 用于模板配置中-数据库表属性
 *
 * @author fox
 */
public enum TableEnum {

    Selected("选择",0),
    No("No",1),
    SqlName("sqlName",2),
    ClassName("className",3),
    ClassNameLower("classNameLower",4),
    Remarks("remarks",5);

    private String name;
    private int order;

    TableEnum(String name, int order) {
        this.name = name;
        this.order = order;
    }

    public static List<String> getTableEnumList(){
        List<String> list = new ArrayList<String>();
        for(TableEnum c : TableEnum.values()){
            list.add(c.getName());
        }
        return list;
    }

    public static String[] getTableEnums(){
        List<String> list = getTableEnumList();
        return list.toArray(new String[list.size()]);
    }

    public static TableEnum get(String name){
        if(name==null){
            return null;
        }
        for(TableEnum c:TableEnum.values()){
            if(name.toLowerCase().equals(c.getName().toLowerCase())){
                return c;
            }
        }
        return null;
    }

    public static TableEnum getByOrder(int order){
        for(TableEnum c:TableEnum.values()){
            if(order==c.getOrder()){
                return c;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public int getOrder() {
        return order;
    }

}
