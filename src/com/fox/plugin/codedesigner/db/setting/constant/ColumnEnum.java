package com.fox.plugin.codedesigner.db.setting.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * ColumnEnum<br>
 * 用于模板配置中-表字段属性
 *
 * @author fox
 */
public enum ColumnEnum {

    No("No",0),
    SqlName("sqlName",1),
    ClassName("sqlTypeName",2),
    Pk("pk",3),
    Remarks("remarks",4),
    ColumnName("columnName",5),
    JavaType("javaType",6),
    ColumnNameLower("columnNameLower",7);

    private String name;
    private int order;

    ColumnEnum(String name, int order) {
        this.name = name;
        this.order = order;
    }

    public static List<String> getColumnEnumList(){
        List<String> list = new ArrayList<String>();
        for(ColumnEnum c : ColumnEnum.values()){
            list.add(c.getName());
        }
        return list;
    }

    public static String[] getColumnEnums(){
        List<String> list = getColumnEnumList();
        return list.toArray(new String[list.size()]);
    }

    public static ColumnEnum get(String name){
        if(name==null){
            return null;
        }
        for(ColumnEnum c:ColumnEnum.values()){
            if(name.toLowerCase().equals(c.getName().toLowerCase())){
                return c;
            }
        }
        return null;
    }

    public static ColumnEnum getByOrder(int order){
        for(ColumnEnum c:ColumnEnum.values()){
            if(order==c.getOrder()){
                return c;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public int getOrder() {
        return order;
    }
}
