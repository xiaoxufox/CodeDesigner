package com.fox.plugin.codedesigner.db.table;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.fox.plugin.codedesigner.db.DataSourceProvider;
import com.fox.plugin.codedesigner.db.table.model.Column;
import com.fox.plugin.codedesigner.db.table.model.Table;
import com.fox.plugin.codedesigner.db.util.DBUtil;

/**
 * 根据数据库表的元数据(metadata)创建Table对象
 * <p>
 * <pre>
 * getTable(sqlName) : 根据数据库表名,得到table对象
 * getAllTable() : 搜索数据库的所有表,并得到table对象列表
 * </pre>
 *
 * @author copy from others
 * @author fox
 */
public class TableFactory {

    private TableFactory() {
    }

    private static class SingletonHolder {
        private static final TableFactory instance = new TableFactory();
    }

    public static final TableFactory getInstance() {
        return SingletonHolder.instance;
    }

    /**
     * 测试连接
     */
    public boolean testConnection(String alias) {
        Connection conn = null;
        try {
            conn = DataSourceProvider.getConnection(alias);
            if (conn != null){
                return true;
            }
        } catch (Exception e) {
            return false;
        } finally {
            DBUtil.close(conn);
        }

        return false;
    }

    /**
     * 根据数据源别名获取解析后的数据库，表，字段信息
     *
     * @param alias 数据源别名
     * @return 返回解析后的数据库，表，字段信息
     */
    public List getAllTables(String alias) {
        Connection conn = DataSourceProvider.getConnection(alias);
        try {
            List<Table> tables = new TableCreateProcessor(conn).getAllTables();
            return tables;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(conn);
        }
    }

    private Table getTable(String alias, String tableName) {
        Table t = null;
        try {
            t = _getTable(alias, tableName);
            if (t == null && !tableName.equals(tableName.toUpperCase())) {
                t = _getTable(alias, tableName.toUpperCase());
            }
            if (t == null && !tableName.equals(tableName.toLowerCase())) {
                t = _getTable(alias, tableName.toLowerCase());
            }
        } catch (Exception e) {
            // ignore
        }

        return t;
    }

    private Table _getTable(String alias, String tableName) throws SQLException {
        if (tableName == null || tableName.trim().length() == 0) {
            throw new IllegalArgumentException("tableName must be not empty");
        }

        Connection conn = DataSourceProvider.getConnection(alias);
        DatabaseMetaData dbMetaData = conn.getMetaData();
        ResultSet rs = dbMetaData.getTables(null, null, tableName, null);
        try {
            while (rs.next()) {
                Table table = new TableCreateProcessor(conn).createTable(rs);
                return table;
            }
        } finally {
            DBUtil.close(conn, rs);
        }
        return null;
    }

    public static class TableCreateProcessor {
        private Connection connection;

        private TableCreateProcessor(Connection connection) {
            this.connection = connection;
        }

        public Table createTable(ResultSet rs) throws SQLException {
            String tableName = null;
            try {
                ResultSetMetaData rsMetaData = rs.getMetaData();
                tableName = rs.getString("TABLE_NAME");
                String remarks = rs.getString("REMARKS");

                Table table = new Table();
                table.setSqlName(tableName);
                table.setRemarks(remarks);

                retriveTableColumns(table);
                return table;
            } catch (SQLException e) {
                throw new RuntimeException("create table object error,tableName:" + tableName, e);
            }
        }

        private List<Table> getAllTables() throws SQLException {
            DatabaseMetaData dbMetaData = connection.getMetaData();
            ResultSet rs = dbMetaData.getTables(null, null, null, null);
            try {
                List<Table> tables = new ArrayList<Table>();
                while (rs.next()) {
                    tables.add(createTable(rs));
                }
                return tables;
            } finally {
                DBUtil.close(rs);
            }
        }

        private DatabaseMetaData getMetaData() {
            return DatabaseMetaDataUtils.getMetaData(connection);
        }

        private void retriveTableColumns(Table table) throws SQLException {
            List primaryKeys = getTablePrimaryKeys(table);
            List columns = getTableColumns(table, primaryKeys);

            for (Iterator i = columns.iterator(); i.hasNext(); ) {
                Column column = (Column) i.next();
                table.addColumn(column);
            }
        }

        /**
         * 获取表的所有字段信息
         */
        private List getTableColumns(Table table, List primaryKeys) throws SQLException {
            // get the columns
            List columns = new LinkedList();
            ResultSet columnRs = getColumnsResultSet(table);
            try {
                while (columnRs.next()) {
                    int sqlType = columnRs.getInt("DATA_TYPE");
                    String sqlTypeName = columnRs.getString("TYPE_NAME");
                    String columnName = columnRs.getString("COLUMN_NAME");
                    String remarks = columnRs.getString("REMARKS");
                    boolean isPk = primaryKeys.contains(columnName);

                    Column column = new Column(table, sqlType, sqlTypeName, columnName, isPk, remarks);
                    columns.add(column);
                }
            } finally {
                DBUtil.close(columnRs);
            }
            return columns;
        }

        /**
         * 从元数据获取字段
         */
        private ResultSet getColumnsResultSet(Table table) throws SQLException {
            return getMetaData().getColumns(null, null, table.getSqlName(), null);
        }

        /**
         * 获取表主键
         */
        private List<String> getTablePrimaryKeys(Table table) throws SQLException {
            // get the primary keys
            List primaryKeys = new LinkedList();
            ResultSet primaryKeyRs = null;
            try {
                primaryKeyRs = getMetaData().getPrimaryKeys(null, null, table.getSqlName());
                while (primaryKeyRs.next()) {
                    String columnName = primaryKeyRs.getString("COLUMN_NAME");
                    primaryKeys.add(columnName);
                }
            } finally {
                DBUtil.close(primaryKeyRs);
            }
            return primaryKeys;
        }
    }

    public static class DatabaseMetaDataUtils {
        public static DatabaseMetaData getMetaData(Connection connection) {
            try {
                return connection.getMetaData();
            } catch (SQLException e) {
                throw new RuntimeException("cannot get DatabaseMetaData", e);
            }
        }

    }
}
