package com.fox.plugin.codedesigner.db.table.model;

import com.fox.plugin.codedesigner.db.util.DBDataTypesUtil;
import com.fox.plugin.codedesigner.util.StringUtil;

/**
 * 用于生成代码的表的字段
 *
 * @author fox
 */
public class Column {

	/**
	 * Reference to the containing table
	 */
	private Table table;

	/**
	 * The java.sql.Types type
	 */
	private int sqlType;

	/**
	 * The sql typename. provided by JDBC driver
	 */
	private String sqlTypeName;

	/**
	 * The name of the column
	 */
	private String sqlName;

	/**
	 * True if the column is a primary key
	 */
	private boolean pk;

	/**
	 * The comments of column
	 */
	private String remarks;

    /**
     * 对应字段生成过程中使用的名称
     */
    private String columnName;

    /**
     * 对应字段java类型
     */
    private String javaType;

    /**
     * 字段名首字母小写
     */
    private String columnNameLower;

    public Column() {
    }

	public Column(Table table, int sqlType, String sqlTypeName,
                  String sqlName, boolean pk, String remarks) {
		if(sqlName == null) {throw new NullPointerException("sqlName must be not null");}
		this.table = table;
		this.sqlType = sqlType;
		this.sqlName = sqlName;
		this.sqlTypeName = sqlTypeName;
        this.remarks = remarks;
        this.pk = pk;

		initOtherProperties();
	}

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public int getSqlType() {
        return sqlType;
    }

    public void setSqlType(int sqlType) {
        this.sqlType = sqlType;
    }

    public String getSqlTypeName() {
        return sqlTypeName;
    }

    public void setSqlTypeName(String sqlTypeName) {
        this.sqlTypeName = sqlTypeName;
    }

    public String getSqlName() {
        return sqlName;
    }

    public void setSqlName(String sqlName) {
        this.sqlName = sqlName;
    }

    public boolean isPk() {
        return pk;
    }

    public void setPk(boolean pk) {
        this.pk = pk;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

	public String getColumnName() {
		return columnName;
	}

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public String getColumnNameLower() {
        return columnNameLower;
    }

    public void setColumnNameLower(String columnNameLower) {
        this.columnNameLower = columnNameLower;
    }

    private void initOtherProperties() {
		this.javaType = DBDataTypesUtil.getPreferredJavaType(getSqlType());
		this.columnName = StringUtil.makeAllWordFirstLetterUpperCase(StringUtil.toUnderscoreName(getSqlName()));
        this.columnNameLower = StringUtil.uncapitalize(getColumnName());
	}
	
}
