package com.fox.plugin.codedesigner.db.table.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fox.plugin.codedesigner.util.StringUtil;

/**
 * 用于生成代码的Table对象.对应数据库的table
 *
 * @author fox
 */
public class Table {

    /**
     * 表名
     */
    private String sqlName;

    /**
     * 表名备注
     */
    private String remarks;

    /**
     * 表名转换的类名
     */
    private String className;

    /**
     * 类名首字母小写
     */
    private String classNameLower;

	List<Column> columns = new ArrayList<Column>();

	Map<String, Column> columnMap = new HashMap<String, Column>();

    public Table() {}

	public String getSqlName() {
		return sqlName;
	}

	public void setSqlName(String sqlName) {
		this.sqlName = sqlName;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void setClassName(String customClassName) {
		this.className = customClassName;
	}

	public String getClassName() {
	    if(StringUtil.isEmpty(className)) {
	        return StringUtil.toJavaClassName(sqlName);
	    }else {
	    	return className;
	    }
	}

    public String getClassNameLower() {
        return StringUtil.uncapitalize(getClassName());
    }

    public void setClassNameLower(String classNameLower) {
        this.classNameLower = classNameLower;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public void addColumn(Column column) {
	    this.columns.add(column);
	    this.columnMap.put(column.getSqlName(), column);
    }

    public Map<String, Column> getColumnMap() {
        return columnMap;
    }

    public void setColumnMap(Map<String, Column> columnMap) {
        this.columnMap = columnMap;
    }
}
