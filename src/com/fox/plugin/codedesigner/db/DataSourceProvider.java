package com.fox.plugin.codedesigner.db;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import com.fox.plugin.codedesigner.setting.SettingFacade;

/**
 * 用于提供生成器的数据源
 * 
 * @author badqiu
 * @author fox
 */
public class DataSourceProvider {

	public synchronized static Connection getNewConnection(String alias) {
		try {
			return getDataSource(alias).getConnection();
		}catch(SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public synchronized static Connection getConnection(String alias) {
		try {
			return getDataSource(alias).getConnection();
		}catch(SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public synchronized static DataSource getDataSource(String alias) {
		return new DriverManagerDataSource(alias);
	}

    /**
     * 内部类，处理数据源
     */
	public static class DriverManagerDataSource implements DataSource {

        /**
         * 数据源别名
         */
	    private String alias;

		private static void loadJdbcDriver(String driverClass) {
			try {
				if(driverClass == null || "".equals(driverClass.trim())) {
					throw new IllegalArgumentException("jdbc 'driverClass' must not be empty");
				}
				Class.forName(driverClass.trim());
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("not found jdbc driver class:["+driverClass+"]",e);
			}
		}
		
		private DriverManagerDataSource(String alias) {
		    this.alias = alias;
		}

		@Override
		public Connection getConnection() throws SQLException {
			loadJdbcDriver(getDriverClass());
			return DriverManager.getConnection(getUrl(),getUsername(),getPassword());
		}

        @Override
		public Connection getConnection(String username, String password) throws SQLException {
			loadJdbcDriver(getDriverClass());
			return DriverManager.getConnection(getUrl(),username,password);
		}

        @Override
		public PrintWriter getLogWriter() throws SQLException {
			throw new UnsupportedOperationException("getLogWriter");
		}

        @Override
		public int getLoginTimeout() throws SQLException {
			throw new UnsupportedOperationException("getLoginTimeout");
		}

        @Override
        public Logger getParentLogger() throws SQLFeatureNotSupportedException {
            throw new UnsupportedOperationException("getParentLogger");
        }

        @Override
        public void setLogWriter(PrintWriter out) throws SQLException {
			throw new UnsupportedOperationException("setLogWriter");
		}

        @Override
		public void setLoginTimeout(int seconds) throws SQLException {
			throw new UnsupportedOperationException("setLoginTimeout");
		}

        @Override
		public <T> T  unwrap(Class<T> iface) throws SQLException {
			if(iface == null) {throw new IllegalArgumentException("Interface argument must not be null");}
			if (!DataSource.class.equals(iface)) {
				throw new SQLException("DataSource of type [" + getClass().getName() +
						"] can only be unwrapped as [javax.sql.DataSource], not as [" + iface.getName());
			}
			return (T) this;
		}

        @Override
		public boolean isWrapperFor(Class<?> iface) throws SQLException {
			return DataSource.class.equals(iface);
		}

		private String getUrl() {
			return SettingFacade.getAllDataSource(this.alias).getUrl();
		}

		private String getUsername() {
			return SettingFacade.getAllDataSource(this.alias).getUserName();
		}

		private String getPassword() {
			return SettingFacade.getAllDataSource(this.alias).getPassword();
		}

		private String getDriverClass() {
			return SettingFacade.getAllDataSource(this.alias).getDriverClassName();
		}

		@Override
		public String toString() {
			return "DataSource: "+"url="+getUrl()+" username="+getUsername();
		}
	}
}
