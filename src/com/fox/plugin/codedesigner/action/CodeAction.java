package com.fox.plugin.codedesigner.action;

import com.fox.plugin.codedesigner.setting.CodeTemplateSettings;
import com.fox.plugin.codedesigner.setting.DataSourceSettings;
import com.fox.plugin.codedesigner.ui.CodeDesignerDialog;
import com.fox.plugin.codedesigner.util.EnvUtil;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.components.ServiceManager;

public class CodeAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        EnvUtil.project = e.getProject();

        DataSourceSettings dataSourceSettings = ServiceManager.getService(DataSourceSettings.class);
        CodeTemplateSettings codeTemplateSettings = ServiceManager.getService(CodeTemplateSettings.class);

        CodeDesignerDialog dialog = new CodeDesignerDialog(dataSourceSettings, codeTemplateSettings);
        dialog.pack();
        dialog.setVisible(true);
    }
}
