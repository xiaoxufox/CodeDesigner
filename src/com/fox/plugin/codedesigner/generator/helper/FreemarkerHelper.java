package com.fox.plugin.codedesigner.generator.helper;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import org.jetbrains.annotations.NotNull;

import com.fox.plugin.codedesigner.util.EnvUtil;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * FreemarkerHelper<br>
 * 使用StringTemplateLoader加载模板
 *
 * @author fox
 */
public class FreemarkerHelper {

    /**
     * 获取freemarker的配置
     */
    public static Configuration getFreeMarkerConfig() {
        Configuration conf = new Configuration();

        StringTemplateLoader stringTemplateLoader = new StringTemplateLoader();
        conf.setTemplateLoader(stringTemplateLoader);

        conf.setNumberFormat("\\#0.\\#\\#\\#\\#\\#");
        conf.setDateTimeFormat("yyyy-MM-dd HH:mm:ss");
        conf.setDateFormat("yyyy-MM-dd");
        conf.setTimeFormat("HH:mm:ss");
        conf.setWhitespaceStripping(true);
        conf.setBooleanFormat("true,false");
        conf.setDefaultEncoding(EnvUtil.getProjectCharset().name());

        return conf;
    }

    /**
     * 将模板内容转换为freemarker的template
     */
    public static Template getTemplate(String templateContent) {
        Configuration conf = getFreeMarkerConfig();
        try {
            return new Template("template", new StringReader(templateContent), conf, EnvUtil.getProjectCharset().name());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解析模板，返回生成的内容
     */
    @NotNull
    public static String parse(String templateContent, Map data) {
        Template template = getTemplate(templateContent);
        StringWriter sw = new StringWriter();
        try {
            template.process(data, sw);
        } catch (TemplateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sw.toString();
    }
}
