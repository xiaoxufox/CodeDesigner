package com.fox.plugin.codedesigner.generator.helper;

import java.io.File;
import java.io.IOException;

import com.intellij.openapi.util.io.FileUtil;

/**
 * FileWriterHelper<br>
 * 文件写入util
 *
 * @author copy from others
 */
public class FileWriterHelper {

    public static boolean write(String filePath, String fileNameWithExt, byte[] contentByte){
        File p = new File(filePath);
        p.mkdirs();
        File f = new File(filePath+fileNameWithExt);
        boolean r = false;
        if(!f.exists()){
            try {
                if(contentByte!=null && contentByte.length>0) {
                    FileUtil.writeToFile(f, contentByte);
                    r = true;
                }else{
                    setMsg("content empty");
                }
            } catch (IOException e) {
                setMsg("exception: "+e.getCause());
            }
        }else{
            setMsg("file exist");
        }
        FileOpenerHelper.openFile(f);
        return r;
    }

    private static String msg;

    public static String getMsg() {
        return msg;
    }

    public static void setMsg(String msg) {
        FileWriterHelper.msg = msg;
    }
}
