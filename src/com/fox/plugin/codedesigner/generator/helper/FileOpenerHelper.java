package com.fox.plugin.codedesigner.generator.helper;

import java.io.File;

import com.fox.plugin.codedesigner.util.EnvUtil;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileManager;

/**
 * FileOpenerHelper<br>
 *
 * @author copy from others
 */
public class FileOpenerHelper {

    public static void openFile(File f){
        try {
            VirtualFileManager.getInstance().syncRefresh();
            VirtualFile vf = LocalFileSystem.getInstance().findFileByIoFile(f);
            if(vf!=null){
                FileEditorManager.getInstance(EnvUtil.project).openFile(vf, false);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
