package com.fox.plugin.codedesigner.generator.helper;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.fox.plugin.codedesigner.db.table.model.Table;
import com.fox.plugin.codedesigner.generator.model.GeneratorModel;
import com.fox.plugin.codedesigner.setting.SettingFacade;
import com.fox.plugin.codedesigner.setting.constant.TemplateTypeEnum;
import com.fox.plugin.codedesigner.setting.model.CodeTemplateModel;
import com.fox.plugin.codedesigner.setting.model.GeneratorDefaultModel;
import com.fox.plugin.codedesigner.util.EnvUtil;
import com.fox.plugin.codedesigner.util.MapUtil;
import com.fox.plugin.codedesigner.util.StringUtil;

/**
 * GeneratorHelper<br>
 * 代码生成helper
 *
 * @author fox
 */
public class GeneratorHelper {

    /**
     * 代码生成
     */
    public static void generate(GeneratorModel generatorModel) {
        List<Table> tables = generatorModel.getTableList();
        List<String> templateList = generatorModel.getTemplateList();

        for (Table table : tables){
            Map<String, Object> templateModel = initTemplateModel(table);
            Map<String, Object> dirModel = initDirModel(templateModel);

            // 处理所有模板生成
            for (String templateName : templateList){
                // 获取模板信息
                CodeTemplateModel template = SettingFacade.getTemplate(templateName);

                // 模板生成文件扩展名
                String defaultTemplateExt = template.getType();

                // 解析后的代码生成内容
                String originalContent = FreemarkerHelper.parse(template.getContent(), templateModel);

                // 解析后的文件相对路径
                String originalFileDir = FreemarkerHelper.parse(templateName, dirModel);

                // 预处理文件目录，获取文件的绝对路径
                String filePath = prepareCheckDir(originalFileDir, defaultTemplateExt);

                // 预处理文件名
                String fileNameWithExt = prepareCheckFileName(originalFileDir, defaultTemplateExt);

                // 文件写入
                FileWriterHelper.write(filePath, fileNameWithExt, originalContent.getBytes(EnvUtil.getProjectCharset()));
            }
        }
    }

    /**
     * 初始化代码生成模板参数
     */
    private static Map<String,Object> initTemplateModel(Table table) {
        Map<String, Object> templateModel = new HashMap<>();
        templateModel.putAll(getShareVarsMap());
        templateModel.put("table", table);;
        templateModel.putAll(BeanHelper.describe(table));

        return templateModel;
    }

    /**
     * 初始化模板路径生成model
     */
    private static Map<String,Object> initDirModel(Map<String, Object> templateModel) {
        Map<String, Object> dirModel = new HashMap<>();
        dirModel.putAll(templateModel);
        // 所有参数添加_dir转换为路径
        dirModel.putAll(StringUtil.getDirValuesMap(templateModel));

        return dirModel;
    }

    /**
     * 获取所有共享变量
     */
    private static Map<String,Object> getShareVarsMap() {
        // 获取默认配置
        GeneratorDefaultModel defaultModel = SettingFacade.getGeneratorDefault();
        Map<String, Object> shareVarsMap = new HashMap<>();

        shareVarsMap.putAll(defaultModel.getCustomerConfigMap());
        shareVarsMap.put("author", defaultModel.getAuthor());
        shareVarsMap.put("outputDir", defaultModel.getTemplateOutPutDir());
        shareVarsMap.putAll(MapUtil.resolveKeyPlaceholder(System.getenv()));
        shareVarsMap.putAll(MapUtil.resolveKeyPlaceholder(System.getProperties()));
        shareVarsMap.put("now", new Date());

        return shareVarsMap;
    }


    /**
     * 预处理文件目录，获取文件的绝对路径
     */
    private static String prepareCheckDir(String originalFileDir, String defaultTemplateExt) {
        // 默认配置输出路径
        String outputDir = SettingFacade.getGeneratorDefault().getTemplateOutPutDir();

        String origianlFileName = prepareCheckFileName(originalFileDir, defaultTemplateExt);
        String fileName = StringUtil.getFileNameNoExtension(origianlFileName, TemplateTypeEnum.getTemplateTypeList());

        int index = originalFileDir.lastIndexOf(fileName);


        String dirDealed =  originalFileDir.substring(0, index).replaceAll("\\.", "/");
        if (dirDealed.endsWith("/")){
            return outputDir + "/" + dirDealed;
        }else {
            return outputDir + "/" + dirDealed + "/";
        }
    }

    /**
     * 预处理文件名
     */
    private static String prepareCheckFileName(String originalFileDir, String defaultTemplateExt) {
        String fileExtension = StringUtil.getExtension(originalFileDir);
        if (StringUtil.isEmpty(fileExtension) || !TemplateTypeEnum.Java.getName().equals(fileExtension)
                || !TemplateTypeEnum.Xml.getName().equals(fileExtension)){
            fileExtension = defaultTemplateExt;
        }

        String originalFileName = StringUtil.getFileNameNoExtension(originalFileDir, TemplateTypeEnum.getTemplateTypeList());

        List<String> splitList = new LinkedList<>();
        splitList.add(".");
        splitList.add("/");
        String fileName = StringUtil.getFileNameRemoveSplit(originalFileName, splitList);

        return fileName+"."+fileExtension;
    }

}
