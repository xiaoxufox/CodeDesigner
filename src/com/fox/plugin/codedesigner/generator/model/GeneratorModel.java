package com.fox.plugin.codedesigner.generator.model;

import java.util.List;

import com.fox.plugin.codedesigner.db.table.model.Table;

/**
 * GeneratorModel<br>
 * 最后要处理的表和模板数据
 *
 * @author fox
 */
public class GeneratorModel {

    /**
     * 生成过程使用模板列表
     */
    private List<String> templateList;

    /**
     * 生成使用的表数据
     */
    private List<Table> tableList;

    public List<String> getTemplateList() {
        return templateList;
    }

    public void setTemplateList(List<String> templateList) {
        this.templateList = templateList;
    }

    public List<Table> getTableList() {
        return tableList;
    }

    public void setTableList(List<Table> tableList) {
        this.tableList = tableList;
    }
}
