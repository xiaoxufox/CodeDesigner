package com.fox.plugin.codedesigner.setting.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于展示模板可以配置的参数表头
 *
 * @author fox
 */
public enum SampleEnum {
    Name("名称",0),
    ParamName("参数名",1),
    ParamType("参数类型",2),
    ParamDefault("默认值",3),
    ParamExplain("配置示例",4);

    private String name;
    private int order;

    SampleEnum(String name, int order) {
        this.name = name;
        this.order = order;
    }

    public static List<String> getSampleList(){
        List<String> list = new ArrayList<String>();
        for(SampleEnum c : SampleEnum.values()){
            list.add(c.getName());
        }
        return list;
    }

    public static String[] getSamples(){
        List<String> list = getSampleList();
        return list.toArray(new String[list.size()]);
    }

    public static SampleEnum get(String name){
        if(name==null){
            return null;
        }
        for(SampleEnum c: SampleEnum.values()){
            if(name.toLowerCase().equals(c.getName().toLowerCase())){
                return c;
            }
        }
        return null;
    }

    public static SampleEnum getByOrder(int order){
        for(SampleEnum c: SampleEnum.values()){
            if(order==c.getOrder()){
                return c;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public int getOrder() {
        return order;
    }
}
