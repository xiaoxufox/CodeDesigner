package com.fox.plugin.codedesigner.setting.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 模板文件类型
 *
 * @author fox
 */
public enum TemplateTypeEnum {
    Java("java",0),
    Xml("xml",1);

    private String name;
    private int order;

    TemplateTypeEnum(String name, int order) {
        this.name = name;
        this.order = order;
    }

    public static List<String> getTemplateTypeList(){
        List<String> list = new ArrayList<String>();
        for(TemplateTypeEnum c : TemplateTypeEnum.values()){
            list.add(c.getName());
        }
        return list;
    }

    public static String[] getTemplateTypes(){
        List<String> list = getTemplateTypeList();
        return list.toArray(new String[list.size()]);
    }

    public static TemplateTypeEnum get(String name){
        if(name==null){
            return null;
        }
        for(TemplateTypeEnum c: TemplateTypeEnum.values()){
            if(name.toLowerCase().equals(c.getName().toLowerCase())){
                return c;
            }
        }
        return null;
    }

    public static TemplateTypeEnum getByOrder(int order){
        for(TemplateTypeEnum c: TemplateTypeEnum.values()){
            if(order==c.getOrder()){
                return c;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public int getOrder() {
        return order;
    }
}
