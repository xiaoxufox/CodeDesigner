package com.fox.plugin.codedesigner.setting.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义配置列表-表头信息
 *
 * @author fox
 */
public enum CustomerConfigColumnEnum {
    No("No",0),
    Key("key",1),
    Value("value",2);

    private String name;
    private int order;

    CustomerConfigColumnEnum(String name, int order) {
        this.name = name;
        this.order = order;
    }

    public static List<String> getCustomerConfigColumnList(){
        List<String> list = new ArrayList<String>();
        for(CustomerConfigColumnEnum c : CustomerConfigColumnEnum.values()){
            list.add(c.getName());
        }
        return list;
    }

    public static String[] getCustomerConfigColumns(){
        List<String> list = getCustomerConfigColumnList();
        return list.toArray(new String[list.size()]);
    }

    public static CustomerConfigColumnEnum get(String name){
        if(name==null){
            return null;
        }
        for(CustomerConfigColumnEnum c: CustomerConfigColumnEnum.values()){
            if(name.toLowerCase().equals(c.getName().toLowerCase())){
                return c;
            }
        }
        return null;
    }

    public static CustomerConfigColumnEnum getByOrder(int order){
        for(CustomerConfigColumnEnum c: CustomerConfigColumnEnum.values()){
            if(order==c.getOrder()){
                return c;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public int getOrder() {
        return order;
    }
}
