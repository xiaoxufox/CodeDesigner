package com.fox.plugin.codedesigner.setting.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据源配置列表-表头信息
 *
 * @author fox
 */
public enum DataSourceColumnEnum {
    No("No",0),
    AliasName("AliasName",1),
    Host("Host",2),
    DataBase("DataBase",3),
    User("User",4),
    Password("Password",5),
    Url("Url",6),
    Driver("Driver",7);

    private String name;
    private int order;

    DataSourceColumnEnum(String name, int order) {
        this.name = name;
        this.order = order;
    }

    public static List<String> getColumnNameList(){
        List<String> list = new ArrayList<String>();
        for(DataSourceColumnEnum c : DataSourceColumnEnum.values()){
            list.add(c.getName());
        }
        return list;
    }

    public static String[] getColumnNames(){
        List<String> list = getColumnNameList();
        return list.toArray(new String[list.size()]);
    }

    public static DataSourceColumnEnum get(String name){
        if(name==null){
            return null;
        }
        for(DataSourceColumnEnum c:DataSourceColumnEnum.values()){
            if(name.toLowerCase().equals(c.getName().toLowerCase())){
                return c;
            }
        }
        return null;
    }

    public static DataSourceColumnEnum getByOrder(int order){
        for(DataSourceColumnEnum c:DataSourceColumnEnum.values()){
            if(order==c.getOrder()){
                return c;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public int getOrder() {
        return order;
    }
}
