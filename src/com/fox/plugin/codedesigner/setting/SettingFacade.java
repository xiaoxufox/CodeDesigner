package com.fox.plugin.codedesigner.setting;

import java.util.Map;

import com.fox.plugin.codedesigner.setting.model.CodeTemplateModel;
import com.fox.plugin.codedesigner.setting.model.DataSourceModel;
import com.fox.plugin.codedesigner.setting.model.GeneratorDefaultModel;
import com.intellij.openapi.components.ServiceManager;

/**
 * SettingFacade<br>
 * 用于获取配置数据源和模板facade
 *
 * @author fox
 */
public class SettingFacade {

    /**
     * 获取数据源Settings
     */
    public static DataSourceSettings getDataSourceSettings(){
        return ServiceManager.getService(DataSourceSettings.class);
    }

    /**
     * 获取所有数据源信息
     */
    public static Map<String, DataSourceModel> getAllDataSource(){
        return ServiceManager.getService(DataSourceSettings.class).getDataSourceMap();
    }

    /**
     * 根据数据源别名获取数据源信息
     */
    public static DataSourceModel getAllDataSource(String aliasName){
        return ServiceManager.getService(DataSourceSettings.class).getDataSource(aliasName);
    }

    /**
     * 获取所有模板信息
     */
    public static Map<String, CodeTemplateModel> getAllTemplate(){
        return ServiceManager.getService(CodeTemplateSettings.class).getTemplateMap();
    }

    /**
     * 获取模板Settings
     */
    public static CodeTemplateSettings getCodeTemplateSettings(){
        return ServiceManager.getService(CodeTemplateSettings.class);
    }

    /**
     * 根据模板名称获取模板
     */
    public static CodeTemplateModel getTemplate(String templateName){
        return ServiceManager.getService(CodeTemplateSettings.class).getTemplate(templateName);
    }

    /**
     * 获取默认值Settings
     */
    public static GeneratorDefaultSettings getGeneratorDefaultSettings(){
        return ServiceManager.getService(GeneratorDefaultSettings.class);
    }

    /**
     * 获取默认配置
     */
    public static GeneratorDefaultModel getGeneratorDefault(){
        return ServiceManager.getService(GeneratorDefaultSettings.class).getGeneratorDefaultModel();
    }

    private SettingFacade(){}
}
