package com.fox.plugin.codedesigner.setting;

import java.util.HashMap;
import java.util.Map;

import org.jetbrains.annotations.Nullable;

import com.fox.plugin.codedesigner.setting.model.DataSourceModel;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.XmlSerializerUtil;

/**
 * DataSourceSettings<br>
 * 存储数据源配置信息
 *
 * @author fox
 */
@State(name = "DataSourceSettings", storages = {@Storage(id = "app-default", file = "$APP_CONFIG$/DataSource-settings.xml")})
public class DataSourceSettings implements PersistentStateComponent<DataSourceSettings> {

    /**
     * 数据源配置信息
     */
    private Map<String, DataSourceModel> dataSourceMap;

    public DataSourceSettings() {
    }

    @Nullable
    @Override
    public DataSourceSettings getState() {
        if (this.dataSourceMap == null) {
            initDataSourceMap();
        }
        return this;
    }

    @Override
    public void loadState(DataSourceSettings dataSourceSettings) {
        XmlSerializerUtil.copyBean(dataSourceSettings, this);
    }

    /**
     * 获取所有配置的数据源
     */
    public Map<String, DataSourceModel> getDataSourceMap() {
        if (this.dataSourceMap == null) {
            initDataSourceMap();
        }
        return this.dataSourceMap;
    }

    public void setDataSourceMap(Map<String, DataSourceModel> dataSourceMap) {
        this.dataSourceMap = dataSourceMap;
    }

    /**
     * 根据配置数据源名称获取完整的数据源配置
     */
    public DataSourceModel getDataSource(String dataSourceName) {
        return this.dataSourceMap.get(dataSourceName);
    }

    /**
     * 根据数据源名称删除数据源配置
     */
    public void removeDataSource(String dataSourceName) {
        this.dataSourceMap.remove(dataSourceName);
    }

    /**
     * 初始化一个空的数据源配置Map
     */
    private void initDataSourceMap() {
        Map<String, DataSourceModel> dataSourceModelMap = new HashMap<String, DataSourceModel>();
        this.dataSourceMap = dataSourceModelMap;
    }

}
