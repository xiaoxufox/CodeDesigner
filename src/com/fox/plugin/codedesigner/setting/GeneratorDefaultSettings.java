package com.fox.plugin.codedesigner.setting;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.fox.plugin.codedesigner.setting.model.GeneratorDefaultModel;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.XmlSerializerUtil;

/**
 * GeneratorDefaultSettings<br>
 * 代码生成过程中使用到的默认配置
 *
 * @author fox
 */
@State(name = "GeneratorDefaultSettings", storages = {@Storage(id = "app-default", file = "$APP_CONFIG$/GeneratorDefault-settings.xml")})
public class GeneratorDefaultSettings implements PersistentStateComponent<GeneratorDefaultSettings> {

    /**
     * 代码生成过程中使用到的默认配置
     */
    private GeneratorDefaultModel generatorDefaultModel = new GeneratorDefaultModel();

    public GeneratorDefaultSettings() {
    }

    @Nullable
    @Override
    public GeneratorDefaultSettings getState() {
        return this;
    }

    @Override
    public void loadState(GeneratorDefaultSettings generatorDefaultModel) {
        XmlSerializerUtil.copyBean(generatorDefaultModel, this);
    }

    public GeneratorDefaultModel getGeneratorDefaultModel() {
        return generatorDefaultModel;
    }

    public void setGeneratorDefaultModel(GeneratorDefaultModel generatorDefaultModel) {
        this.generatorDefaultModel = generatorDefaultModel;
    }

    @NotNull
    public static GeneratorDefaultModel getGeneratorDefault() {
        try {
            return ServiceManager.getService(GeneratorDefaultSettings.class).generatorDefaultModel;
        }catch (Exception e){
            return new GeneratorDefaultModel();
        }
    }


}
