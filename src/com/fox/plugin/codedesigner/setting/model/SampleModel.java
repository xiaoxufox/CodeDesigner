package com.fox.plugin.codedesigner.setting.model;

/**
 * SampleModel<br>
 * 用于展示模板可以配置的参数
 *
 * @author fox
 */
public class SampleModel {

    /**
     * 名称
     */
    private String name;

    /**
     * 参数名
     */
    private String paramName;

    /**
     * 参数类型
     */
    private String paramType;

    /**
     * 默认值
     */
    private String paramDefault;

    /**
     * 配置示例
     */
    private String paramExplain;

    public SampleModel(String name) {
        this.name = name;
    }

    public SampleModel(String name, String paramName, String paramType, String paramDefault, String paramExplain) {
        this.name = name;
        this.paramName = paramName;
        this.paramType = paramType;
        this.paramDefault = paramDefault;
        this.paramExplain = paramExplain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getParamDefault() {
        return paramDefault;
    }

    public void setParamDefault(String paramDefault) {
        this.paramDefault = paramDefault;
    }

    public String getParamExplain() {
        return paramExplain;
    }

    public void setParamExplain(String paramExplain) {
        this.paramExplain = paramExplain;
    }

    @Override
    public String toString() {
        return name;
    }
}
