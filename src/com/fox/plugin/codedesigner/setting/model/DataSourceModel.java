package com.fox.plugin.codedesigner.setting.model;

/**
 * DataSourceModel<br>
 * 数据源
 *
 * @author fox
 */
public class DataSourceModel {

    /**
     * 数据源别名
     */
    private String aliasName;

    /**
     * 主机名
     */
    private String hostIp;

    /**
     * 数据库
     */
    private String dbName;

    /**
     * 数据库用户名
     */
    private String userName;

    /**
     * 数据库密码
     */
    private String password;

    /**
     * 数据连接url
     */
    private String url;

    /**
     * 数据库驱动类名
     */
    private String driverClassName = "com.mysql.jdbc.Driver";

    public DataSourceModel() {
    }

    public DataSourceModel(String aliasName, String hostIp, String dbName, String userName, String password, String url, String driverClassName) {
        this.aliasName = aliasName;
        this.hostIp = hostIp;
        this.dbName = dbName;
        this.userName = userName;
        this.password = password;
        this.url = url;
        this.driverClassName = driverClassName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /**
     * 这里是为了在界面展示方便，只显示别名
     */
    @Override
    public String toString() {
        return aliasName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataSourceModel that = (DataSourceModel) o;

        if (aliasName != null ? !aliasName.equals(that.aliasName) : that.aliasName != null) return false;
        if (hostIp != null ? !hostIp.equals(that.hostIp) : that.hostIp != null) return false;
        if (dbName != null ? !dbName.equals(that.dbName) : that.dbName != null) return false;
        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        return driverClassName != null ? driverClassName.equals(that.driverClassName) : that.driverClassName == null;
    }

    @Override
    public int hashCode() {
        int result = aliasName != null ? aliasName.hashCode() : 0;
        result = 31 * result + (hostIp != null ? hostIp.hashCode() : 0);
        result = 31 * result + (dbName != null ? dbName.hashCode() : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (driverClassName != null ? driverClassName.hashCode() : 0);
        return result;
    }
}
