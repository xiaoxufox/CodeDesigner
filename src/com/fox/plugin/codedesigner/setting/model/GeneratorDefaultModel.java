package com.fox.plugin.codedesigner.setting.model;

import java.util.HashMap;
import java.util.Map;

/**
 * GeneratorDefaultModel<br>
 * 代码生成过程中使用到的默认配置
 *
 * @author fox
 */
public class GeneratorDefaultModel {

    /**
     * 默认表名去除前缀
     */
    private String tableRemovePrefix;

    /**
     * 作者
     */
    private String author = "admin";

    /**
     * 模板生成路径
     */
    private String templateOutPutDir;

    /**
     * 自定义参数
     */
    private Map<String, String> customerConfigMap = new HashMap<String, String>();


    public GeneratorDefaultModel() {
    }

    public GeneratorDefaultModel(String tableRemovePrefix, String author) {
        this.tableRemovePrefix = tableRemovePrefix;
        this.author = author;
    }

    public String getTableRemovePrefix() {
        return tableRemovePrefix;
    }

    public void setTableRemovePrefix(String tableRemovePrefix) {
        this.tableRemovePrefix = tableRemovePrefix;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTemplateOutPutDir() {
        return templateOutPutDir;
    }

    public void setTemplateOutPutDir(String templateOutPutDir) {
        this.templateOutPutDir = templateOutPutDir;
    }

    public Map<String, String> getCustomerConfigMap() {
        return customerConfigMap;
    }

    public void setCustomerConfigMap(Map<String, String> customerConfigMap) {
        this.customerConfigMap = customerConfigMap;
    }

    public void addCustomerConfig(String key, String value){
        this.customerConfigMap.put(key, value);
    }
}
