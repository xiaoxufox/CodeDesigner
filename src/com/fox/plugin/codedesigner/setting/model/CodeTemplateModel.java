package com.fox.plugin.codedesigner.setting.model;

/**
 * CodeTemplateModel<br>
 * 代码模板
 *
 * @author fox
 */
public class CodeTemplateModel {

    /**
     * 模板名称
     */
    private String templateName;

    /**
     * 模板内容
     */
    private String content;

    /**
     * 模板类型
     */
    private String type;

    public CodeTemplateModel() {
    }

    public CodeTemplateModel(String templateName, String content, String type) {
        this.templateName = templateName;
        this.content = content;
        this.type = type;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * 这里只放一个字段是为了给界面list展示用
     */
    @Override
    public String toString() {
        return templateName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CodeTemplateModel that = (CodeTemplateModel) o;

        if (templateName != null ? !templateName.equals(that.templateName) : that.templateName != null) {
            return false;
        }
        if (content != null ? !content.equals(that.content) : that.content != null) {
            return false;
        }
        return type != null ? type.equals(that.type) : that.type == null;
    }

    @Override
    public int hashCode() {
        int result = templateName != null ? templateName.hashCode() : 0;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
