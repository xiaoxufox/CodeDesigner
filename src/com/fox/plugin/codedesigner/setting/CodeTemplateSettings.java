package com.fox.plugin.codedesigner.setting;

import java.util.HashMap;
import java.util.Map;

import org.jetbrains.annotations.Nullable;

import com.fox.plugin.codedesigner.setting.model.CodeTemplateModel;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.XmlSerializerUtil;

/**
 * DataSourceSettings<br>
 * 存储模板配置信息
 *
 * @author fox
 */
@State(name = "CodeTemplateSettings", storages = {@Storage(id = "app-default", file = "$APP_CONFIG$/CodeTemplate-settings.xml")})
public class CodeTemplateSettings implements PersistentStateComponent<CodeTemplateSettings> {

    /**
     * 模板配置信息
     */
    private Map<String, CodeTemplateModel> templateMap;

    public CodeTemplateSettings() {
    }

    @Nullable
    @Override
    public CodeTemplateSettings getState() {
        if (this.templateMap == null) {
            initTemplateMap();
        }
        return this;
    }

    @Override
    public void loadState(CodeTemplateSettings codeTemplateSettings) {
        XmlSerializerUtil.copyBean(codeTemplateSettings, this);
    }

    public Map<String, CodeTemplateModel> getTemplateMap() {
        return templateMap;
    }

    public void setTemplateMap(Map<String, CodeTemplateModel> templateMap) {
        this.templateMap = templateMap;
    }

    /**
     * 根据名称获取完整的模板
     */
    public CodeTemplateModel getTemplate(String templateName) {
        return this.templateMap.get(templateName);
    }

    /**
     * 根据名称删除模板
     */
    public void removeTemplate(String templateName) {
        this.templateMap.remove(templateName);
    }

    /**
     * 初始化一个空的配置Map
     */
    private void initTemplateMap() {
        this.templateMap = new HashMap<String, CodeTemplateModel>();
    }

}
